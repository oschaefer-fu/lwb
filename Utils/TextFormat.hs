-- Paket zur Formatierung von Text in  der Konsole unter Verwen-
-- dung von Ansi-Sequenzen und Ausrichtung von Text
-- Das Paket vereint die Funktionalitäten des ehemaligen Ansiseq
-- Pakets mit den  für eine 'schöne' Konsolenausgabe notwendigen
-- Funktionen rjustify, ljustify und cjustify aus dem ehemaligen
-- PreludeExt-Paket.
-- Portierung  der Miranda-Version vom Februar 2016 nach Haskell
-- (c) Oliver Schäfer, LWB-Informatik, FU-Berlin, im Januar 2020

module LWB.Utils.TextFormat (
  Color (..), Att (..), Common (..),
  vfaerben, hfaerben, fett, attribut, positioniere, loesche,
  rjustify, ljustify, cjustify
) where

data Sequenz = F Color | B Color | A Att | C Common
data Color   = Schwarz | Rot | Gruen | Gelb | Blau | Violett | Hellblau | Weiss
data Att     = Fett | Normal | Blinkend | Unterstrichen
data Common  = Reset | Loeschen

vfaerben,hfaerben :: [Char] -> Color -> [Char]
vfaerben text farbe = ansiseq (F farbe) ++ text ++ ansiseq (C Reset)
hfaerben text farbe = ansiseq (B farbe) ++ text ++ ansiseq (C Reset)

fett :: [Char] -> [Char]
fett text = attribut text Fett

attribut :: [Char] -> Att -> [Char]
attribut text a = ansiseq (A a) ++ text ++ ansiseq (A Normal)

positioniere :: Int -> Int -> [Char]
positioniere x y = ['\27'] ++ "[" ++ show x ++ ";" ++ show y ++ "H"

loesche :: [Char]
loesche = ansiseq (C Loeschen)

ansiseq :: Sequenz -> [Char]
ansiseq = esc . ansiseq'
          where
          esc = (['\27'] ++)
          ansiseq' (F Schwarz)       = "[30m"
          ansiseq' (F Rot)           = "[31m"
          ansiseq' (F Gruen)         = "[32m"
          ansiseq' (F Gelb)          = "[33m"
          ansiseq' (F Blau)          = "[34m"
          ansiseq' (F Violett)       = "[35m"
          ansiseq' (F Hellblau)      = "[36m"
          ansiseq' (F Weiss)         = "[37m"
          ansiseq' (B Schwarz)       = "[40m"
          ansiseq' (B Rot)           = "[41m"
          ansiseq' (B Gruen)         = "[42m"
          ansiseq' (B Gelb)          = "[43m"
          ansiseq' (B Blau)          = "[44m"
          ansiseq' (B Violett)       = "[45m"
          ansiseq' (B Hellblau)      = "[46m"
          ansiseq' (B Weiss)         = "[47m"
          ansiseq' (A Normal)        = "[0m"
          ansiseq' (A Fett)          = "[1m"
          ansiseq' (A Blinkend)      = "[1m"
          ansiseq' (A Unterstrichen) = "[1m"
          ansiseq' (C Reset)         = "[0;49m"
          ansiseq' (C Loeschen)      = "[2J"

cjustify :: Int -> [Char] -> [Char]
cjustify n xs = spaces lmargin ++ xs ++ spaces rmargin
                where
                lmargin = (n - length xs) `div` 2
                rmargin = n - length xs - lmargin

rjustify :: Int -> [Char] -> [Char]
rjustify n xs = spaces margin ++ xs
                where
                margin = n - length xs

ljustify :: Int -> [Char] -> [Char]
ljustify n xs = xs ++ spaces margin
                where
                margin = n - length xs

spaces :: Int -> [Char]
spaces n = replicate n ' '
