module LWB.DBS.Topsort where
import Data.List (sort, nub)

-- Implementierung eines Verfahrens zur topologischen Sortierung einer Liste von Präzedenzen
-- (c) Oliver Schäfer                        18.12.2018, portiert nach Haskell am 02.12.2019

-- INPUT:   [(t,t)] Liste von Präzedenzen
-- OUTPUT:  [t]     topologisch sortierte Liste der Knoten
--          Spezialfall [], falls wegen Zyklen keine Sortierung möglich

-- 1. Schritt: Erstellen des Präzedenzgraphen
--             aus der Liste der Präzedenzen
pgraph :: Eq t => [(t,t)] -> ([t],[(t,t)])
pgraph ps = ((nub . concat) [[n1,n2]|(n1,n2)<-ps],ps)

-- 2. Schritt: Topologisches Sortieren des Präzedenzgraphen
--             Der Graph G besteht aus einer Knotenliste ns (nodes)
--             und einer Kantenliste es (edges): G = (ns,es
--             Das Mitführen der Knotenliste ist wichtig, damit beim Entfernen
--             von Präzedenzen (Kanten des Graphs) kein Knoten verloren geht.
tsort :: Eq t => ([t],[(t,t)]) -> [t]
tsort (ns,es)
  = tsort' (ns,es) []
    where
    tsort' (ns,es) akk
      | null es = akk ++ ns                       -- Kantenliste leer, nur 'freie' Knoten
      | null fs = []                              -- Graph enthält Zyklen, kein 'freier' Knoten
      | otherwise = tsort' (ns',es') (akk ++ [f]) -- hier passiert was
        where
        f  = head fs
        fs = [n|n<-(nub . map fst) es,null ((filter (==n) . map snd) es)]
        ns' = filter (/=f) ns
        es' = filter p es
              where
              p (ni,nj) = (ni /= f) && (nj /= f)

-- Das eigentliche topologische Sortieren geschieht dann durch Anwenden von tsort auf
-- den Präzedenzgraphen, so dass topsort direkt auf die Liste der Präzedenzen angewendet
-- werden kann.
topsort :: Eq t => [(t,t)] -> [t]
topsort = tsort . pgraph

-- --------------------------------------------------------------------------------------

-- 1. Beispiel: Anziehreihenfolge
kleidung = [("Unterhemd", "Pullover"),
            ("Unterhose", "Hose"),
            ("Pullover", "Mantel"),
            ("Hose", "Mantel"),
            ("Hose", "Schuhe"),
            ("Socken", "Schuhe")]

-- 2. Beispiel: Präzedenzen dargestellt durch [char]
--              Beispiel topologisch sortierbar
p1 = [("T1","T2"),("T2","T3")]

-- 3. Beispiel: Präzedenzen dargestellt durch algebraischen Datentyp
--              Beispiel enthält Zyklus, daher keine topologische Sortierung möglich
data Transaktionen = T Int
                     deriving (Show, Eq)
p2 = [(T 1,T 2),(T 2,T 1)]
