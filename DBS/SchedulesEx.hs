module LWB.DBS.SchedulesEx where
import LWB.DBS.Schedules

--  ---------------------------------------------------------------------
--  Definitionen
t1,t2 :: TA
t1 = [Rd 1 A, Rd 1 B]
t2 = [Wr 2 B, Rd 2 A, Wr 2 B]

--  s1 ist konfliktserialisierbar
--     Beispiel 1 im Skript auf Seite 6-21
--  s2 ist nicht konfliktserialisierbar
--  s3 ist konfliktserialisierbar
--     (wie s1, nur T2 im äquivalenten Schedule vorn)
--  s4 ist nicht konfliktserialisierbar
--     Beispiel 2 im Skript auf Seite 6-21/6-24
--  s5 ist konfliktserialisierbar
--     Beispiel 1 im Skript auf Seite 6-23
s1,s2,s3,s4,s5 :: Sched
s1 = [Rd 1 A, Wr 1 A, Rd 2 A, Wr 2 A,
      Rd 1 B, Wr 1 B, Rd 2 B, Wr 2 B]
s2 = [Rd 1 A, Wr 1 A, Rd 2 A, Wr 2 A,
      Rd 1 B, Wr 1 A, Rd 2 B, Wr 2 B]
s3 = [Rd 2 A, Wr 2 A, Rd 1 A, Wr 1 A,
      Rd 2 B, Wr 2 B, Rd 1 B, Wr 1 B]
s4 = [Rd 1 A, Wr 1 A, Rd 2 A, Wr 2 A, 
      Rd 2 B, Wr 2 B, Rd 1 B, Wr 1 B]
s5 = [Rd 2 A, Rd 1 B, Wr 2 A, Rd 3 A, 
      Wr 1 B, Wr 3 A, Rd 2 B, Wr 2 B]

--  Schedules vom Übungszettel
--  sA: S  aus Übungsaufgabe 20.2
--  sB: S  aus Übungsaufgabe 20.3
--  sC: S1 aus Übungsaufgabe 20.4
--  sD: S2 aus Übungsaufgabe 20.4
--  sE: S3 aus Übungsaufgabe 20.4
--  sF: S4 aus Übungsaufgabe 20.4
sA,sB,sC,sD,sE,sF :: Sched
sA = [Rd 1 X, Rd 2 X, Wr 2 X, Rd 2 Y,
      Rd 2 U, Rd 3 X, Rd 4 U, Wr 3 V,
      Wr 4 U, Rd 5 V, Rd 5 Y, Wr 2 Y]
sB = [Rd 1 X, Rd 2 X, Rd 2 Y, Wr 3 Z,
      Wr 3 T, Rd 1 Z, Rd 4 T, Wr 1 Z,
      Rd 4 Y, Wr 4 T, Wr 4 U, Rd 5 Z,
      Rd 5 V, Rd 6 U, Rd 6 T, Rd 6 V, Wr 7 V]
sC = [Rd 1 Z, Rd 6 X, Rd 5 V, Rd 3 T,
      Wr 4 X, Wr 6 U, Rd 2 U, Wr 5 U,
      Wr 4 Y, Wr 3 Y, Rd 2 Z, Rd 2 Y,
      Wr 2 X, Wr 2 Y, Rd 1 Y, Wr 1 Z]
sD = [Wr 4 X, Wr 4 Y, Rd 6 X, Wr 6 U,
      Rd 2 U, Rd 2 Y, Rd 2 Z, Wr 2 X,
      Wr 2 Y, Rd 3 T, Wr 3 Y, Rd 5 V,
      Wr 5 U, Rd 1 Z, Rd 1 Y, Wr 1 Z]
sE = [Rd 2 U, Rd 1 Z, Rd 1 Y, Rd 5 V,
      Wr 1 Z, Rd 3 T, Rd 2 Z, Wr 4 X,
      Wr 5 U, Rd 6 X, Rd 2 Y, Wr 2 X,
      Wr 6 U, Wr 2 Y, Wr 4 Y, Wr 3 Y]
sF = [Rd 2 U, Rd 1 Z, Rd 1 Y, Rd 5 V,
      Wr 1 Z, Rd 3 T, Rd 2 Z, Wr 4 X,
      Wr 5 U, Rd 2 Y, Wr 2 X, Rd 6 X,
      Wr 6 U, Wr 2 Y, Wr 4 Y, Wr 3 Y]
