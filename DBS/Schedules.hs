module LWB.DBS.Schedules where
--  Konfliktserialisierbarkeit                 c) Oliver Schäfer
--  Version vom 20.01.2018,  nach Haskell portiert am 02.12.2019

--  Ein Schedule ist eine Liste elementarer DBS-Operationen, die
--  im vereinfachten R/W-Modell aus der Angabe der Zugehörigkeit
--  zu einer Transaktion i, der Art a (lesen 'Rw' oder schreiben
--  'Wr') und der Angabe des Datenelementes d (hier A-Z) besteht.

--  Definition der Aufzähltypen durch algebraischen Datentypen
--  Datenbankelemente A-Z

import Data.List (sort, nub, (\\))

data Dbe = A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z
           deriving (Show,Eq)
--  Zugriffstyp lesen (Rd) oder schreiben (Wr)
data Eop = Rd Int Dbe | Wr Int Dbe
           deriving (Show)
--  Transaktionen und Schedules sind prinzipiell vom gleichen Typ
--  (Folge von Elementaroperationen). Transaktionen haben die zu-
--  sätzliche Eigenschaft, genau eine Transaktionsnummer aufzuweisen.
type TA    = [Eop]
type Sched = [Eop]

--  Prüft, ob eine gegebene Folge von Elementaroperationen zu
--  genau einer Transaktion gehört
istTA :: [Eop] -> Bool
istTA = (1==) . length . nub . (map idx)

--  Liefert den Transaktionsindex der Elementaroperation
--  Hier hilft der anonyme Bezeichner, den es in Miranda
--  nicht gibt.
idx :: Eop -> Int
idx (Rd i e) = i
idx (Wr i e) = i

--  Prädikat, ob gegebene DB-Operationen vertauschbar sind
--  Siehe Skript Satz 6.5.1
tauschbar :: Eop -> Eop -> Bool
tauschbar (Rd i1 _)  (Rd i2 _)  =  i1 /= i2
tauschbar (Rd i1 e1) (Wr i2 e2) = (i1 /= i2) && (e1 /= e2)
tauschbar (Wr i1 e1) (Rd i2 e2) = (i1 /= i2) && (e1 /= e2)
tauschbar (Wr i1 e1) (Wr i2 e2) = (i1 /= i2) && (e1 /= e2)

--  Ein Präzedenzgraph wird repräsentiert durch das Paar (V,E), bestehend aus
--  einer Liste V::[Int] von Knoten und einer Liste E::[(Int,Int)] von Kanten.

--  Liefert den Präzedenzgraphen eines Schedule
pgraph :: Sched -> ([Int],[(Int,Int)])
pgraph ops
  = ((sort . nub . map idx) ops, (sort . nub . kanten) ops)
    where
    kanten []       = []
    kanten (op:ops) = praez op ops ++ kanten ops
    praez op [] = []
    praez op (op':ops)
      | not (tauschbar op op') && idx op /= idx op' = (idx op,idx op'):praez op ops
      | otherwise                                   = praez op ops

--  Ermittelt eine topologische Sortierung des (zyklenfreien) Präzedenzgraphen
--  Algorithmus: Entferne solange Knoten aus dem Präzedenzgraphen, die keinen
--               Vorgänger haben, bis die Knotenliste leer ist.
tsort :: Sched -> [Int]
tsort s
  | hasCycles s = error "Präzedenzgraph nicht zykelfrei"
  | otherwise   = tsort' (pgraph s)
    where
    tsort' ([],[]) = []
    tsort' g = node:tsort' g'
               where node = head (noPredList g)
                     g'   = dropPredGraph node g

--  Liefert die Liste aller Kanten des Präzedenzgraphen, in der es keinen
--  Knoten ohne Vorgänger mehr gibt.
cycles :: Sched -> [(Int,Int)]
cycles  s
  = cycles' (pgraph s)
    where
    cycles' g
      | null (noPredList g) = snd g
      | otherwise           = cycles' g'
        where
        node = head (noPredList g)
        g'   = dropPredGraph node g

--  Liefert die Liste der vorgängerlosen Knoten
noPredList :: ([Int],[(Int,Int)]) -> [Int]
noPredList g = fst g \\ ((nub . map snd) (snd g))

--  Liefert den Graphen, der entsteht, wenn man den Knoten entfernt
dropPredGraph :: Int -> ([Int],[(Int,Int)]) -> ([Int],[(Int,Int)])
dropPredGraph node g = (fst g \\ [node], filter ((node/=) . fst) (snd g))

--  Prädikat, ob ein Präzedenzgraph Zyklen enthält
hasCycles :: Sched -> Bool
hasCycles = not . null . cycles

--  Liefert für den Fall, dass s konfliktserialisierbar ist, einen
--  konfliktäquivalenten seriellen Schedule.
serialize :: Sched -> Sched
serialize s
  | hasCycles s = []
  | istTA s     = s
  | otherwise   = ser' taList s
    where taList = tsort s
          ser' []     s = []
          ser' (v:vs) s = filter ((v==) . idx) s ++ ser' vs s
