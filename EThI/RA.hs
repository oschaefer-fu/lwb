module LWB.EThI.RA (
  allOf, oneOf, raOr, raAnd, raStar, raPlus, ra0or1, raMaxN, raMinN,
  raN, raMN, matches, showMatches, version,
  alpha_, digit_, upper_, lower_, alnum_, punct_
) where

import Data.List (sort, nub)
import LWB.EThI.Automaten hiding (version)
import LWB.Utils.TextFormat

allOf    :: [Char] -> [Char] -> RA
-- Vor.: xs enthält nur druckbare  ASCII-Zeichen sowie Tabulatoren '\t' und
--       Zeichenendenzeichen '\n'.
-- Erg.: In 'allOf name xs' ist ein RA geliefert, der auf die Zeichenket-
--       te xs passt.
--       Für name /= "" ist der Bezeichner  des RA der Wert von name, sonst
--       xs.

oneOf    :: [Char] -> [Char] -> RA
-- Vor.: xs enthält nur druckbare ASCII-Zeichen  sowie Tabulatoren '\t' und
--       Zeienendenzeichen '\n'.
-- Erg.: In 'oneOf name xs' ist ein  RA geliefert, der auf irgendeines der
--       Zeichen aus xs passt.
--       Für  name /= "" ist der Bezeichner des RA der Wert von name, sonst
--       (x0|x1|x2|...|xn-1) mit name = [x0,x1,x2,...,xn-1].

raOr     :: [RA] -> RA
-- RA:   [r1,r1,r3,...] -> r1|r2|r3|...
-- Vor.: keine
-- Erg.: In 'raOr [r1,r2,r3,...]' ist ein RA geliefert, der auf alle Wörter
--       w passt, auf die r1 ODER r2 ODER r3 ODER ... passt.
--       Der Bezeichner des RA ist aus den Bezeichnern von r1,r2,r3,... zu-
--       sammengesetzt mit eingeschobenen '|'.

raAnd    :: [RA] -> RA
-- RA:   [r1,r2,r3,...] -> r1r2r3...
-- Vor.: keine
-- Erg.: In 'raAnd [r1,r2,r3,...]' ist  ein  RA geliefert, der auf  all die
--       Wörter w1w2w3 passt, für die r1 auf w1 UND r2 auf w2 UND r3 auf w3
--       UND ... passen.
--       Der Bezeichner des RA ist aus den Bezeichnern von r1,r2,r3,... zu-
--       sammengesetzt (konkateniert).

raStar   :: RA -> RA
-- RA:   r -> r*
-- Vor.: keine
-- Erg.: In 'raStar r' ist ein RA geliefert, der  auf beliebige, auch kein-
--       malige Wiederholungen von Wörtern passt, auf die r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'raStar r' <r>*, ansonsten (<r>)*.

raPlus   :: RA -> RA
-- RA:   r -> r+
-- Vor.: keine
-- Erg.: In 'raStar r' ist ein RA geliefert, der  auf  beliebige, aber min-
--       destens einmalige Wiederholungen von Wörtern passt, auf die der RA
--       r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'raPlus r' <r>+, ansonsten (<r>)+.

ra0or1   :: RA -> RA
-- RA:   r -> r?
-- Vor.: keine
-- Erg.: In 'raStar r' ist ein  RA geliefert, der  auf kein- oder einmalige
--       Wiederholung von Wörtern passt, auf die r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'ra0or1 r' <r>?, ansonsten (<r>)?.

raMaxN   :: Int -> RA -> RA
-- RA:   n -> r -> r{,n}
-- Vor.: keine
-- Erg.: In 'raMaxN n r' ist ein RA geliefert, der auf kein- bis maximal n-
--       malige Wiederholung von Wörtern passt, auf die r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'raMaxN r' <r>{,n}, ansonsten (<r>){,n}.

raMinN   :: Int -> RA -> RA
-- RA:   r -> r -> r{n,}
-- Vor.: keine
-- Erg.: In 'raMinN n r' ist ein RA  geliefert, der auf mindestens n-malige
--       Wiederholung von Wörtern passt, auf die r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'raMinN r' <r>{n,}, ansonsten (<r>){n,}.

raN      :: Int -> RA -> RA
-- RA:   n -> r -> r{n}
-- Vor.: keine
-- Erg.: In 'raN n r' ist ein RA  geliefert, der auf genau n-malige Wieder-
--       holung von Wörtern passt, auf die r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'raN r' <r>{n}, ansonsten (<r>){n}.

raMN     :: Int -> Int -> RA -> RA
-- RA:   m -> n -> r -> r{m,n}
-- Vor.: keine
-- Erg.: In 'raMN m n r' ist ein  RA  geliefert,  der auf mindestens n- und
--       höchstens m-malige Wiederholung von Wörtern passt, auf die r passt.
--       Besteht der Bezeichner <r> von r  nur aus  einem  Zeichen, ist der
--       zeichner von 'raMN r' <r>{m,n}, ansonsten (<r>){m,n}.

matches  :: RA -> [Char] -> Bool
-- Vor.: keine
-- Erg.: In 'matches r xs' ist genau dann True geliefert, wenn der RA r auf
--       xs passt.

showMatches :: [(RA,[Char])] -> [[Char]] -> IO ()
-- Vor.: keine
-- Erg.: In 'showMatches rss wss' ist für jeden RA r<-map fst rss und  jede
--       Zeichenkette  w<-zss getestet,  ob w auf r  passt und das Ergebnis
--       dieser Auswertung auf dem Bildschirm  ausgegeben. Die  Ausgabe ist
--       in Tabellenform erfolgt, die Tabellenköpfe sind die den  RAs zuge-
--       ordneten Zeichenketten.

version  :: IO ()
-- Vor.: keine
-- Erg.: Ein Informationstext zur aktuellen Versionsnummer ist geliefert.

alpha_  :: RA
--      RA, der auf ein beliebigen Buchstaben matcht.
digit_  :: RA
--      RA, der auf eine beliebige Dezimalziffer matcht.
upper_  :: RA
--      RA, der auf einen beliebigen Großbuchstaben matcht (ohne Umlaute).
lower_  :: RA
--      RA, der auf einen beliebigen Kleinbuchstaben matcht (ohne Umlaute).
alnum_  :: RA
--      RA, der auf einen beliebigen Buchstaben oder Dezimalziffer matcht.
punct_  :: RA
--      RA, der auf ein Zeichen aus ".,:;?!" matcht.

-- ----------------------------------------------------------------------
-- Reguläre Ausdrücke auf der Basis der Haskell-Paketes 'LWB.Automaten'
-- (c) Oliver Schäfer                                         2015 - 2019
-- ----------------------------------------------------------------------

newtype RA = R (Automat,[Char])

version = do
  let string = "RA-Interpreter v0.43 vom 14.09.2019"
  putStrLn string

alpha_ = oneOf "[:alpha:]"  letters
digit_ = oneOf "[:digits:]" digits
alnum_ = oneOf "[:alnum:]"  (digits ++ letters)
punct_ = oneOf "[:punct:]"  ".,:;!?"
upper_ = oneOf "[:upper:]"  capitals
lower_ = oneOf "[:lower:]"  small

raMaxN n (R r)
  | length (snd r) > 1 = R ((fst.deR) r', "(" ++ snd r ++ "){," ++
                           Prelude.show n ++ "}")
  | otherwise          = R ((fst.deR) r',        snd r ++  "{," ++
                           Prelude.show n ++ "}")
    where
    r' = raOr ([ra0or1 (R r)] ++ [raAnd (replicate k (R r))|k<-[2..n]])

raMinN n (R r)
  = R ((fst.deR) r', f (snd r))
    where
    f name | length name < 2 || bekannt name = name ++  "{" ++
                                               Prelude.show n ++ ",}"
           | otherwise                       = "(" ++ name ++ "){" ++
                                               Prelude.show n ++ ",}"
    r' | n==0      = raStar (R r)
       | n==1      = raPlus (R r)
       | otherwise = raAnd [raAnd (replicate (n-1) (R r)), raPlus (R r)]

raN n (R r)
  | length (snd r) < 2 || bekannt (snd r) = R ((fst.deR) r',        snd r ++
                                              "{" ++ Prelude.show n ++ "}")
  | otherwise                             = R ((fst.deR) r', "(" ++ snd r ++
                                              "){" ++ Prelude.show n ++ "}")
    where
    r' = raAnd (replicate n (R r))

raMN m n (R r)
  | length (snd r) < 2 || bekannt (snd r) = R ((fst.deR) r',        snd r ++
                                              "{" ++ Prelude.show m ++ "," ++
                                                     Prelude.show n ++ "}")
  | otherwise                             = R ((fst.deR) r', "(" ++ snd r ++
                                              "){" ++ Prelude.show m ++ "," ++
                                                      Prelude.show n ++ "}")
    where
    r' = raOr [raAnd (replicate k (R r))|k<-[m..n]]

raPlus (R (r,name))
  | length name < 2 || bekannt name = R ((fst.deR) (raAnd [R (r,""),raStar (R (r,name))]),        name ++  "+")
  | otherwise                       = R ((fst.deR) (raAnd [R (r,""),raStar (R (r,name))]), "(" ++ name ++ ")+")

ra0or1 (R (r,name))
  | length name < 2 || bekannt name = R (ra0or1' r,        name ++  "?")
  | otherwise                       = R (ra0or1' r, "(" ++ name ++ ")?")
    where
    ra0or1' a
      = neaE qs' zs' ts'
        where
        qs' = map (shiftQ 1 . chgStart . chgAccept) qsA ++ [(n,Accept)]
        qsA = states a
        n   = maximum (map fst qsA) + 2
        shiftQ x (q,t) = (q+x,t)
        chgAccept (n,Accept) = (n,Inner)
        chgAccept (n,t)      = (n,t)
        chgStart  (0,Start)  = (0,Inner)
        chgStart  (n,t)      = (n,t)
        zs' = sigma a
        tsA = transitions a
        ts' = map (shiftT 1) tsA ++ [(0,"",1),(0,"",n)] ++ [(k+1,"",n)|(k,Accept)<-qsA]
        shiftT x (qi,z,qj) = (qi+x,z,qj+x)

raStar (R (r,name))
  | length name < 2 || bekannt name = R (raStar' r,        name ++  "*")
  | otherwise                       = R (raStar' r, "(" ++ name ++ ")*")
    where
    raStar' a
      = neaE qs' zs' ts'
        where
        qs' = map (shiftQ 1 . chgStart . chgAccept) qsA ++ [(n,Accept)]
        qsA = states a
        n   = maximum (map fst qsA) + 2
        shiftQ x (q,t) = (q+x,t)
        chgAccept (n,Accept) = (n,Inner)
        chgAccept (n,t)      = (n,t)
        chgStart  (0,Start)  = (0,Inner)
        chgStart  (n,t)      = (n,t)
        zs' = sigma a
        tsA = transitions a
        ts' = map (shiftT 1) tsA ++ [(0,"",1),(0,"",n),(n,"",0)] ++ [(k+1,"",n)|(k,Accept)<-qsA]
        shiftT x (qi,z,qj) = (qi+x,z,qj+x)

raOr rs
  = R (foldr1 raOr' (map (fst.deR) rs), "(" ++ (concat . (intersperse "|") . (map (snd.deR))) rs ++ ")")
    where
    raOr' a b
      = neaE qs' zs' ts'
        where
        qs' = (map (shiftQ 1) . map chgStart) qsA ++ (map (shiftQ n) . map chgStart) qsB
        qsA = states a
        qsB = states b
        n   = maximum (map fst qsA) + 2
        shiftQ x (q,t) = (q+x,t)
        chgStart (0,Start) = (0,Inner)
        chgStart (n,t)     = (n,t)
        zs' = (sort . nub) (sigma a ++ sigma b)
        tsA = transitions a
        tsB = transitions b
        ts' = map (shiftT 1) tsA ++ map (shiftT n) tsB ++ [(0,"",1),(0,"",n)]
        shiftT x (qi,z,qj) = (qi+x,z,qj+x)

raAnd rs
  = R (foldr1 raAnd' (map (fst.deR) rs), (concat . (intersperse "") . (map (snd.deR))) rs)
    where
    raAnd' a b
      = neaE qs' zs' ts'
        where
        qs' = map chgAccept qsA ++ (map (shiftQ n) . map chgStart) qsB
        qsA = states a
        qsB = states b
        n   = maximum (map fst qsA) + 2
        shiftQ x (q,t) = (q+x,t)
        chgAccept (n,Accept) = (n,Inner)
        chgAccept (n,t)      = (n,t)
        chgStart  (0,Start)  = (0,Inner)
        chgStart  (n,t)      = (n,t)
        zs' = (sort . nub) (sigma a ++ sigma b)
        tsA = transitions a
        tsB = transitions b
        ts' = tsA ++ map (shiftT n) tsB ++ [(k,"",n)|(k,Accept)<-qsA]
        shiftT x (qi,z,qj) = (qi+x,z,qj+x)

matches = accepts . fst . deR

allOf name xs
  | name /= "" = R (sucher xs, name)
  | otherwise  = R (sucher xs, xs)
    where
    sucher b = nea qs zs ts
               where 
               qs = [(i,Inner)|i<-[1..length b-1]] ++ [(length b,Accept)]
               zs = ascii
               ts = [(i,[b!!i],i+1)|i<-[0..length b-1]]

oneOf name xs
  | name /= "" = R (nea qs zs ts, name)
  | otherwise  = R (nea qs zs ts, name')
    where
    name' | length xs > 1 = (("("++) . (++")")) (intersperse '|' xs)
          | otherwise     = xs
    qs = [(0,Start)] ++ [(i,Accept)|i<-[1..length xs]]
    zs = ascii
    ts = [(0,[xs!!i],i+1)|i<-[0..length xs-1]]

instance Show RA where
  show = snd . deR

showMatches ads ws
  = do  let string = "   " ++ pl "Zeichenkette" ++ " | " ++  ds                        ++ "\n" ++
                     "   " ++ replicate ml '-'  ++ "-+-" ++  replicate (length ds) '-' ++ "\n" ++
                     showMatches' as ws pl pr
        putStrLn string
        where
        as = map fst ads
        ds = concat (map (pr . snd) ads)
        ml = max (maximum (map length ws)) (length "Zeichenkette")
        mr = max (length "nein") (maximum (map (length . snd) ads))
        pl = rjustify ml
        pr = (" "++) . (++" ") . (ljustify mr)
        showMatches' as []     pl pr = []
        showMatches' as (w:ws) pl pr
          = "   " ++ pl w ++ " | " ++ concat (map (pr . f) as) ++ "\n" ++ showMatches' as ws pl pr
            where
            f a | matches a w = "ja  "
                | otherwise   = "nein"

-- ------------------------ Hilfsfunktionen -----------------------------
deR :: RA -> (Automat,[Char])
deR (R r) = r

intersperse :: t -> [t] -> [t]
intersperse x xs = (tail . concat) [[x,y]|y<-xs]

bekannt :: [Char] -> Bool
bekannt name = head name == '[' && last name == ']' || head name == '(' && last name == ')'
