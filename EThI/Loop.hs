-- ====================== LOOP-Programme ========================
-- (c) Oliver Schäfer, FU-Berlin, LWB Informatik        2014-2019
-- Version vom Sa 14. Sep 09:59:23 CEST 2019 (Haskell-Portierung)
-- ==============================================================

module LWB.EThI.Loop (
  Memory, Adr (..), succ, pred, get, set, copy, inc, dec,
  loop, comp, noop, macro11, macro12, macro21, macro22,
  program, result
) where

-- ============================ LOOP-Programme ===============================
-- (c) Oliver Schäfer                                                2014-2019
-- Es  werden imperative, d.h. zustandsorientierte, Programme funktional nach-
-- gebaut, die die Semantik von LOOP-Programme haben (siehe Vorlesungsskript).
-- ===========================================================================
-- Loop.hs stellt durch Einfügen der  Anweisung 'import LWB.EThI.Loop' die Ba-
-- sis-Elemente einer LOOP-Sprache zur Verfügung.
--
-- Ein LOOP-Programm hat den Aufbau
-- <name> <parameterliste> =
--   program [
--     <Liste von Anweisungen, durch ',' getrennt>
--   ]
--
-- Das Ergebnis eines Programmablaufes ist der Speichervektor, der nach  Abar-
-- beitung  der letzten Anweisung  vorliegt. Eine Anweisung arbeitet auf einem
-- Speichervektor. Jedes Programm beginnt mit dem leeren Speichervektor sigma0
-- (x0,x1,...x15) = (0,0,...,0).
--
-- Folgende Sprachelemente stehen zur Verfügung:
--
-- (1) Wertzuweisungen:
--     (a) Variablen belegen: xi := <c>
--         set X<i> <c>
--     (b) Variableninhalte kopieren: xj := xi
--         copy X<i> X<j>
--     (c) Erhöhen eines Variablenwertes: xi := succ (xi)
--         inc X<i>
--     (d) Erniedrigen eines Variablenwertes: xi := pred (xi)
--         dec X<i>
-- (2) Loop-Schleife: loop xi do P1;P2;... end
--     loop X<i> [
--       P1, P2, ...
--     ]
-- (3) Verwendung n-stelliger Makros mit einem oder zwei Rückgabewerten:
--     (a) Soll nur ein skalarer Rückgabewert (aus X0!) des Makros m verwendet
--         werden, verwendet man den Aufruf  'X<j> macro<i>1 m X<i1> [X<i2>]',
--         das den Rückgabewert nach X<j> kopiert und die/den Eingabeparameter
--         für das Makro m aus den Registern X<i1> (und X<i2>) nimmt.
--     (b) Liefert das Makro ein  Rückgabewertepaar (stets X1 und X2), so ver-
--         endet man den Aufruf 'X<j1> X<j2> macro<i>2 m X<i1> [X<i2>]'. Dabei
--         werden  die Rückgabewerte in die Register <X<j1> und X<j2> kopiert.
--     Nur die beiden Registerxn X<j1> (bzw. X<j2>) werden ggf. verändert.
-- (4) Ausgabe des Ergebnisses: x0 = f(x1,...,xn)
--     result (<name> <parameterliste>)

import Prelude hiding (succ,pred)

-- ------------------------------- Definitionen ------------------------------
-- Speichervektor, eingeschränkt auf natürliche Zahlen 
type Memory = [Int]
-- Anweisung, Abbildung eines Speichervektors auf einen anderen
type Instruction = Memory -> Memory
-- Speichergröße: 16 Bytes (X0..X15)
size =  16
-- Speicheradressen als alg. Datentyp, ggf. an 'size' anpassen
data Adr = X0  | X1  | X2  | X3  | X4  | X5  | X6  | X7  |
           X8  | X9  | X10 | X11 | X12 | X13 | X14 | X15
           deriving (Show,Enum)
-- --------------------------------------------------------------

succ, pred :: Int -> Int
succ n = n+1
pred 0 = 0
pred n = n-1

-- Um aus einer Adresse einen Speicherindex zu generieren
idx :: Adr -> Int
idx = fromEnum

-- Speicherinhalt holen oder setzen
get :: Adr -> Memory -> Int
get xi ram = ram!!(idx xi)

set :: Adr -> Int -> Memory -> Memory
set xi n ram = take (idx xi) ram ++ [n] ++ drop (idx xi+1) ram

-- Speicherinhalt holen und setzen in einer Anweisung
getNsetM :: Adr -> Adr -> (Int -> Int) -> Memory -> Memory
getNsetM adr0 adr1 p ram = set adr1 (p (get adr0 ram)) ram

-- der besseren Lesbarkeit wegen ...
copy n m = getNsetM n m id
inc  n   = getNsetM n n succ
dec  n   = getNsetM n n pred

-- Schleifen
-- mit Variableninhalt als Schleifendurchlaufzahl
loop :: Adr -> [Instruction] -> Memory -> Memory
loop n fs ram = (iterate (comp fs) ram)!!(get n ram)

comp :: [Instruction] -> Instruction
comp []     = noop
comp (f:fs) = (comp fs) . f

-- leere Operation
noop :: Memory -> Memory
noop = id

-- Ein Programm ist eine Anweisungsfolge, die auf einem initial leeren Speicher arbeitet
program :: [Instruction] -> Memory
program is = program' (replicate size 0,is)
             where
             program' (ram, [])   = ram
             program' (ram, f:fs) = program' (f ram, fs)

-- Funktion, um das Ergebnis aus x0 zu extrahieren
result = (!!0)

-- erlaubt die Anwendung n-stelliger Makros mit n<-{1,2}, die entweder einen
-- oder zwei Rückgabewerte liefern. Im Fall eines Rückgabewertes wird dieser
-- aus X0 extrahiert, bei zwei Rückgabewerten aus X1 und X2.
macro11 out1      f in1     ram
  = set out1 (result (f (get in1 ram)))               ram
macro21 out1      f in1 in2 ram
  = set out1 (result (f (get in1 ram) (get in2 ram))) ram
macro12 out1 out2 f in1     ram
  = comp [ set out1 (erg!!1), set out2 (erg!!2) ]     ram
    where erg = f (get in1 ram)
macro22 out1 out2 f in1 in2 ram
  = comp [ set out1 (erg!!1), set out2 (erg!!2) ]     ram
    where erg = f (get in1 ram) (get in2 ram)
