module LWB.EThI.Whilemakros where

import LWB.EThI.While

-- Addiert zwei Zahlen, Ergebnis in x0
add x y =
  program [
    set  X1 x,  -- Parameterübergabe Summand 1 an Speichervektor
    set  X2 y,  -- Parameterübergabe Summand 2 an Speichervektor
    copy X2 X0, -- x0 := x2;
    loop X1 [   -- loop x1 do
      inc X0    --   x0 := succ (x0)
    ]           -- end
  ]

-- Multipliziert zwei Zahlen unter Verwendung des add-Makros, Ergebnis in x0
mul x y =
  program [
    set X1 x,               -- Parameterübergabe Faktor 1 an Speichervektor
    set X2 y,               -- Parameterübergabe Faktor 2 an Speichervektor
    loop X2 [               -- loop x2 do;
      macro21 X0 add X1 X0   --   x0 := x1 + x0
    ]                       -- end
  ]

-- Berechnet die Potenz x^y unter Verwendung des mul-Makros, Ergebnis in x0
pow x y =
  program [
    set X1 x,               -- Parameterübergabe Basis an Speichervektor
    set X2 y,               -- Parameterübergabe Exponent an Speichervektor
    set X0 1,               -- x0 := 1
    loop X2 [               -- loop x2 do;
      macro21 X0 mul X1 X0   --   x0 := x1 * x0
    ]                       -- end
  ]

-- Berechnet die Differenz x-y für x>=y, Ergebnis in x0
sub x y =
  program [
    set X0 x,
    set X1 y,
    loop X1 [
      dec X0
    ]
  ]
          
-- Berechnet das Ergebnis der Ganzzahldivision x div y
divide x y =
  program [
     set X1 x, 
     set X2 y,
     loop X1 [
       dec X2,
       loop X2 [    -- x verringern um (y-1)
         dec X1
       ],
       inc X2,
       -- simuliert 'if x1 > 0 then P endif'
       -- unter Verwendung der Hilfsvariablen x3
       set X3 0,
       loop X1 [
         set X3 1
       ],
       loop X3 [    -- P
         inc X0,    -- Zähler um 1 erhöhen
         dec X1     -- x verringern um 1
       ]
     ]
   ]

modulo x y =
  program [
    set X1 x,
    set X2 y,
    macro21 X3 divide X1 X2,
    macro21 X3 mul X3 X2,
    macro21 X0 sub X1 X3
  ]
