module LWB.EThI.Loopmakros where

import LWB.EThI.Loop

-- Addiert zwei Zahlen
-- Ergebnis in x0
add x y =
  program [
    set  X1 x,  -- Parameterübergabe Summand 1 an Speichervektor
    set  X2 y,  -- Parameterübergabe Summand 2 an Speichervektor
    copy X2 X0,             -- x0 := x2;
    loop X1 [               -- loop x1 do
      inc X0                --   x0 := succ (x0)
    ]                       -- end
  ]

-- Multipliziert zwei Zahlen unter Verwendung des Additions-Makros
-- Ergebnis in x0
mul x y =
  program [
    set X1 x,   -- Parameterübergabe Faktor 1 an Speichervektor
    set X2 y,   -- Parameterübergabe Faktor 2 an Speichervektor
    loop X2 [               -- loop x2 do;
      macro21 X0 add X1 X0  --   x0 := x1 + x0
    ]                       -- end
  ]

-- Berechnet die Potenz x^y unter Verwendung des Multiplikations-Makros
-- Ergebnis in x0
pow x y =
  program [
    set X1 x,   -- Parameterübergabe Basis an Speichervektor
    set X2 y,   -- Parameterübergabe Exponent an Speichervektor
    set X0 1,               -- x0 := 1
    loop X2 [               -- loop x2 do;
      macro21 X0 mul X1 X0  --   x0 := x1 * x0
    ]                       -- end
  ]

-- Berechnet die Differenz x-y für x>=y
-- Ergebnis in x0
sub x y =
  program [
    set X0 x,
    set X1 y,
    loop X1 [
      dec X0
    ]
  ]
          
-- Berechnet das Ergebnis der Ganzzahldivision x div y
-- Ergebnis in X0
ldiv x y =
  program [
    set X1 x, 
    set X2 y,
    loop X1 [
      dec X2,
      loop X2 [    -- x verringern um (y-1)
        dec X1
      ],
      inc X2,
      -- simuliert 'if x1 > 0 then P endif'
      -- unter Verwendung der Hilfsvariablen x3
      set X3 0,
      loop X1 [
        set X3 1
      ],
      loop X3 [    -- P
        inc X0,    -- Zähler um 1 erhöhen
        dec X1     -- x verringern um 1
      ]
    ]
  ]

mini x y =
  program [
    set X1 x,
    set X2 y,
    macro21 X3 sub X1 X2,
    -- Gilt hier x3 = 0, dann ist x <= y, andernfalls y < x
    -- simuliere 'if x3 = 0 then P1 else P2 endif'
    set X5 0,
    set X6 1,
    loop X3 [
      set X5 1,
      set X6 0
    ],
    loop X6 [ -- P1, also x <= y, returniere x1
      copy X1 X0
    ],
    loop X5 [ -- P2, also  y > x, returniere x2
      copy X2 X0
    ]
  ]

maxi x y =
  program [
    set X1 x,
    set X2 y,
    macro21 X3 sub X1 X2,
    -- Gilt hier x3 = 0, dann ist x <= y, andernfalls y < x
    -- simuliere 'if x3 = 0 then P1 else P2 endif'
    set X5 0,
    set X6 1,
    loop X3 [
      set X5 1,
      set X6 0
    ],
    loop X6 [ -- P1, also x <= y, returniere x2
      copy X2 X0
    ],
    loop X5 [ -- P2, also  y > x, returniere x11
      copy X1 X0
    ]
  ]

-- cantor x y berechnet den Wert der Cantorschen Paarungsfunktion
-- unter Verwendung der Makros zur Addition 'add' und Potenzierung
-- 'pow'.
cantor x y =
  program [
    -- Addition von x und y, x0 := x+y
    set X0 x,
    set X1 y,
    macro21 X0 add X0 X1,
    -- Berechnung der Potenz, x0 := 2^x0
    set  X1 2,
    macro21 X0 pow X1 X0,
    -- Addition, x0 := x + x0
    set X1 x,
    macro21 X0 add X1 X0
  ]

-- cantorinverse x berechnet zum Wert der Cantorschen Paarungsfunktion
-- cantor (x1,x2) das Paar (x1,x2) mit Hilfe von Makros.
cantorinverse x =
  program [
    set  X0 x,
    loop X0 [
      -- Berechnung der Potenz x9 := 2*2^x3
      set X1 2,
      macro21 X9 pow X1 X3,
      macro21 X9 mul X1 X9,
      -- Test, ob x0-2*2^x3 >= 0 <=> 1+x0-2*2^x3 > 0 gilt
      -- Dazu Berechnung von x8 := 1+x0-2*2^x3
      set X8 1,
      macro21 X8 add X8 X0,
      macro21 X8 sub X8 X9,
      -- Realisiert die bedingte Anweisung
      -- if x8 > 0 then x3 := succ (x3)
      -- Benutzt die Speicherzellen x7,x8
      set X7 0,
      loop X8 [
        set X7 1
      ],
      loop X7 [
        inc X3
      ]
    ],
    -- Berechnung der Potenz, x9 := 2^x3
    set X1 2,
    macro21 X9 pow X1 X3,
    -- Berechnung von x1 := x0 - 2^x3
    macro21 X1 sub X0 X9,
    -- Berechnung von x2 := x3 - x1
    macro21 X2 sub X3 X1
  ]
