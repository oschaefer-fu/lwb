import LWB.EThI.Automaten

-- DEA, der Worte erkennt, die aus einer geraden Anzahl O's
-- und einer geraden Anzahl T's bestehen.

d1 = dea  [(0,Accept),            -- Liste der Zustände
           (3,Inner),
           (2,Inner),
           (1,Inner)]
           "OT"                   -- Alphabet
           [(0,"O",2),(0,"T",1),  -- Liste der Übergänge
            (1,"O",3),(1,"T",0),
            (2,"O",0),(2,"T",3),
            (3,"O",1),(3,"T",2)]
