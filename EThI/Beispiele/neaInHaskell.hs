import LWB.EThI.Automaten

-- Autor: Oliver Schäfer
-- Datum: Do 12. Sep 23:04:07 CEST 2019
-- Zweck: Einfacher NEA, der *LWB* erkennt.

n1 = nea [(1,Inner),      -- Liste der Zustände
          (2,Inner),
          (3,Accept)]
         capitals         -- Alphabet
         [(0,capitals,0), -- Liste der Übergänge
          (0,"L",1),
          (1,"W",2),
          (2,"B",3),
          (3,capitals,3)]
