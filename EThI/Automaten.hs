{-
  ========================= Haskell-Paket 'Automaten' =======================
  (c) Oliver Schäfer                                                2014-2020
  ===========================================================================
  Erstellt als Fortführung des Miranda-Paketes 'automaten' aus der  Vorlesung
  zur Theoretischen Informatik, nachdem im WS 2019/20 in der ALP1 auf Haskell
  umgestellt wurde.

  Folgende Typen werden zur Zeit zur Verfügung gestellt:

  Deterministische, endliche Automaten                                  (DEA)
  Nicht-deterministische, endliche Automaten                            (NEA)
  Nicht-deterministische, endliche Automaten mit Epsilon-Übergängen    (NEAe)

  Zustände eines Automaten  sind durch ihre Nummer und einen Typ gekennzeich-
  net. Es gibt drei  verschiedene Typen  von Zuständen: Den Startzustand, den
  inneren Zustand (Inner) und den akzeptierenden Endzustand (Accept). Der Zu-
  stand  mit der Nummer 0 ist IMMER der Startzustand (0,Start). Andere Start-
  zustände sind nicht erlaubt.
  Die Ausgabefunktion vearbeitet  nur die 100 Zustände q0 - q99 'sauber', bei
  Indizes >=100 erscheinen die Zustände der Tabelle u. U. nicht mehr gefluch-
  tet; der Korrektheit der Funktionen tut das keinen Abbruch.
-}

module LWB.EThI.Automaten (
  Automat, Typ (..), State, Alphabet, Character, Word, Index, Transition,
  digits, binary, capitals, small, whitespace, interpunkt, math, letters,
  ascii, dea, nea, neaE, transitions, states, sigma, isdea, isnea, isneaE,
  accepts, startFrom, addTransition, changeTransition, deleteTransition,
  addState, changeState, deleteState, showAccepted, version, deleps,
  powersetConstr, equivalent, minimize, unreachable, reduce,
  showConfigurations, writeToHaskellFile
) where

import Prelude hiding (Word)
import Data.List (sort, (\\), nub)
import LWB.Utils.TextFormat

data Typ = Inner | Accept | Start deriving (Show,Eq)
-- Für Anwender nur Inner | Accept
type State = (Int,Typ)
-- Bsp.: (3,Inner) oder (0,Accept)

-- Alphabete setzen sich aus Zeichen vom Typ char  zusammen und haben den Typ
-- [char]. Ein (Eingangs-) Wort setzt sich aus Zeichen  zusammen und muss ein
-- Element der Kleen'schen  Hülle des Alphabets  sein, hat daher den gleichen
-- Typ wie dieses.
--
type Character = Char
type Alphabet  = [Character]
type Word      = [Character]
type Index     = Int

-- Definition spezieller Alphabete, die  bei  der Konstruktion von  Automaten
-- verwendet werden können.

digits, binary, capitals, small, whitespace :: Alphabet
interpunkt, math, letters, ascii            :: Alphabet
digits     = "0123456789"
binary     = "01"
capitals   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
small      = "abcdefghijklmnopqrstuvwxyz"
whitespace = ['\t',' ']
interpunkt = "!?.,:;_"
math       = "+-*/^"
letters    = capitals ++ small
ascii      = letters ++ digits ++ whitespace ++ interpunkt ++ math

-- Ein Übergang wird dargestellt durch ein Tripel (qi,a,qj) <- Q x Sigma x Q,
-- mit den Zuständen qi, qj <- Q und dem Zeichen a<-Sigma des Alphabets. Eine
-- Zustandsübergangsfunktion delta ist eine Menge solcher Übergänge.
-- Da auch nichtdeterministische endliche Automaten implementiert werden sol-
-- len können, ist das a des Tripels kein Zeichen, sondern eine Zeichenkette.
-- So können auch leere (Epsilon-)Übergänge realisiert oder komfortabel Über-
-- gänge zusammengefasst werden. Für DEAs sind  Zeichenfolgen aber immer ein-
-- elementig, also z.B. ['a'] = "a".

type Transition = (Index,Word,Index)

-- Beispiele:
-- (1) Standard-Übergang:                                           (1,"a",2)
--     Erläuterung: typischer DEA-Übergang
--                  auch bei NEA möglich
-- (2) Kurzform für mehrere Übergänge:                             (1,"01",2)
--     Erläuterung: ersetzt (1,"0",2),(1,"1",2)
--                  auch bei DEA möglich
-- (3) Mehrdeutiger Übergang:                             (1,"0",2),(1,"0",1)
--     Erläuterung: Beispiel hier (q_1,'0') -> {q1_q2}
--                  nur bei NEA oder NEAeps möglich
-- (4) Epsilon-Übergang:                                             (1,"",2)
--     Erläuterung: Übergang ohne Konsumierung eines Zeichens
--                  nur bei NEAeps möglich

--         Q         Sigma         delta
dea :: [State] -> Alphabet -> [Transition] -> Automat
-- Vor.: In 'dea qs zs ts' existiert für jedes Paar (q,z)<-qs x zs genau  ein
--       Tripel (q,z,q') <-ts. Es  gibt KEINEN  ANDEREN Zustand q <- qs außer
--       (0,Start), der den Typ 'Start' hat. Für  jeden Zustand q <- qs gilt,
--       dass  er GENAU EINEN Typ hat. Der Zustand mit Index 0 kann auch  den
--       Typ Accept haben.
-- Erg.: 'dea qs zs ts' liefert  einen  deterministischen Automaten   mit der
--       Zustandsmenge qs, dem Alphabet (=Zeichenmenge) ts und den  Zustands-
--       übergängen ts. Die Menge der  Endzustände  ist aus qs abgeleitet und
--       umfasst all   diejenigen Zustände, die  als  'Accept' gekennzeichnet
--       sind. Der Startzustand ist immer derjenige mit dem Index 0.

--         Q         Sigma         delta
nea :: [State] -> Alphabet -> [Transition] -> Automat
-- Vor.: Es gibt KEINEN ANDEREN Zustand q <- qs außer  (0,Start), der den Typ
--       'Start' hat. Für jeden Zustand q <- qs gilt, dass er GENAU EINEN Typ
--       hat. Der Zustand mit Index 0  kann auch  den Typ 'Accept'  haben. Es
--       gibt keinen Übergang (qi,"",qj) (Epsilon-Übergang) in ts.
-- Erg.: 'nea qs zs ts' liefert  einen nicht-deterministischen  Automaten mit
--       der Zustandsmenge qs,  dem  Alphabet (=Zeichenmenge) ts und  den Zu-
--       standsübergängen ts. Die Menge der Endzustände ist aus qs abgeleitet
--       und umfasst all diejenigen Zustände, die  als 'Accept'  gekennzeich-
--       net sind. Der Startzustand ist immer derjenige mit Index 0.

--         Q         Sigma         delta
neaE :: [State] -> Alphabet -> [Transition] -> Automat
-- Vor.: Siehe 'nea'.
--       Außerdem dürfen nun Übergänge der Form (qi,"",qj) (epsilon-Übergänge
--       angegeben werden.
-- Erg.: Liefert einen nicht-deterministischen, endlichen Automaten mit Epsi-
--       lon-Übergängen.

transitions :: Automat -> [Transition]
-- Vor.: -
-- Erg.: Liefert die Liste aller Übergänge des Automaten.

states :: Automat -> [State]
-- Vor.: -
-- Erg.: Liefert die Menge aller Zustände des Automaten.

sigma :: Automat -> Alphabet
-- Vor.: -
-- Erg.: Liefert das Alphabet des Automaten.

isdea :: Automat -> Bool
-- Vor.: -
-- Erg.: G.d.w. in 'isdea a' a ein deterministischer, endlicher Automat  ist,
--       ist True geliefert.

isnea :: Automat -> Bool
-- Vor.: -
-- Erg.: G.d.w. in 'isnea a' a   ein nicht-deterministischer, endlicher Auto-
--       mat ist, ist True geliefert.

isneaE :: Automat -> Bool
-- Vor.: -
-- Erg.: G.d.w. in 'isneaE a' a ein  nicht-deterministischer, endlicher Auto-
--       mat (mit Epsilon-Übergängen) ist, ist True geliefert.

accepts :: Automat -> Word -> Bool
-- Vor.: Das Wort ist in der Kleenschen Hülle des Automatenalphabets  enthal-
--       ten.
-- Erg.: 'accepts a w' liefert genau dann 'True', wenn a nach Abarbeitung des
--       Wortes w in einem Endzustand terminiert.

startFrom :: Automat -> Index -> Word -> [State]
-- Vor.: Das  Wort ist in der Kleenschen Hülle des Automatenalphabets enthal-
--       ten.
-- Erg.: In 'startFrom a n ws' ist die Menge der Zustände q geliefert, in dem
--       sich der Automat - im Zustand mit Index n beginnend - befindet, wenn
--       das Eingabewort ws abgearbeitet ist. Gilt 'isdea a == True', enthält
--       die Rückgabeliste sicher nur einen Zustand.

addTransition :: Automat -> Transition -> Automat
-- Vor.: In 'addTransition a (qi,z,qj)' gilt   sowohl, dass z<-alphabet a als
--       auch qi,qj<-map fst zustaende. Es gibt  keinen Übergang (qi',z',qj')
--       in a, für den gilt: qi'==qi und z'==z (siehe changeTransition).
-- Erg.: 'addTransistion a t' liefert  einen Automaten,  der in allen  Eigen-
--       schaften mit a übereinstimmt und zusätzlich den Übergang t enthält.

changeTransition :: Automat -> Transition -> Automat
-- Vor.: In 'changeTransition a (qi,z,qj)'  ist  z <- alphabet a und es exis-
--       tiert ein qj' <- map fst (zustaende a), so  dass  die  Enthaltensbe-
--       ziehung (qi,z,qj') <- transitions a gilt.
-- Erg.: Geliefert ist ein Automat, der in allen   Eigenschaften  mit a über-
--       einstimmt und für den (qi,z,qj) <- transitions a  gilt,  aber  nicht
--       (qi,z,qj') <- transitions a.

deleteTransition :: Automat -> Index -> Word -> Automat
-- Vor.: keine
-- Erg.: In 'deleteTransition a n z' ist der Automat  geliefert, der in allen
--       Eigenschaften  mit a  übereinstimmt. Wenn a einen  Übergang (n,z,qj)
--       für  irgendeinen  Zustand qj <- map fst (zustaende a)  enthielt, ist
--       das in 'deleteTransition a n z' nicht der Fall.

deleteState :: Automat -> Index -> Automat
-- Vor.: n > 0 (Startzustand kann nicht gelöscht werden)
-- Erg.: In 'deleteState a n' ist  ein Automat geliefert, der in allen Eigen-
--       schaften mit a übereinstimmt, für  den aber nicht die  Enthaltensbe-
--       ziehung n <- map fst (zustaende a) gilt  und für den es keinen Über-
--       gang (qi,z,qj) gibt, mit qi == n oder qj == n.

addState :: Automat -> State -> Automat
-- Vor.: In 'addState a q' gilt  sowohl fst q /= 0 als  auch  snd q /= Start.
--       'fst q' ist nicht in 'map fst (zustaende a)' enthalten.
-- Erg.: 'addState a q' stimmt in  allen  Eigenschaften  mit  dem Automaten a
--       überein, hat aber einen weiteren (unerreichbaren) Zustand q.

changeState :: Automat -> Index -> Typ -> Automat
-- Vor.: In 'changeState a n t' gilt n<-map fst (zustaende a) und t /= Start.
-- Erg.: Geliefert ist ein  Automat,  der in allen Eigenschaften  mit a über-
--       einstimmt, der Typ des Zustandes mit dem Index n ist t.

showAccepted :: [(Automat,[Char])] -> [Word] -> IO ()
-- Vor.: Alle Automaten haben (mindestens) einen  akzeptierenden  Endzustand.
--       In der Eingabeliste ist jedem Automat a eine  beschreibende Zeichen-
--       kette za zugeordnet.
-- Erg.: In 'showAccepted ass wss' ist für jeden  Automat a<-map fst  ass und
--       jedes Wort w<-zss getestet, ob  der Automat das Wort  akzeptiert und
--       das Ergebnis dieser Auswertung  auf  dem Bildschirm  ausgegeben. Die
--       Ausgabe ist in Tabellenform erfolgt, die Tabellenköpfe  sind die den
--       Automaten zugeordneten Zeichenketten.

deleps :: Automat -> Automat
-- Vor.: Der Automat ist ein NEA oder ein NEAe.
-- Erg.: In 'deleps a' ist ein zu a äquivalenter, d.h. ein  die gleiche Spra-
--       che akzeptierender Automat ohne Epsilon-Übergänge geliefert.

powersetConstr :: Automat -> Automat
-- Vor.: Der Automat ist ein NEA oder ein NEAe.
-- Erg.: In 'powersetConstr a' ist der zu a äquivalente, d.h. der die gleiche
--       Sprache akzeptierende DEA geliefert. Der Automat ist nicht notwendi-
--       gerweise minimal.

equivalent :: Automat -> [(Index,Index)]
-- Vor.: Der Automat ist ein DEA.
-- Erg.: Eine Liste von Paaren äquivalenter  Zustände (qi,qj) mit qi > qj ist
--       geliefert.

minimize :: Automat -> Automat
-- Vor.: Der Automat ist ein DEA.
-- Erg.: In 'minimize a' ist der zu a äquivalente, d.h. der die gleiche Spra-
--       che akzeptierende deterministische Automat geliefert, der eine mini-
--       male Anzahl Zustände besitzt.

unreachable :: Automat -> [Index]
-- Vor.: keine
-- Erg.: In 'unreachable a' ist i eine Liste  von Zustandsindizes  geliefert,
--       die von q0 aus nicht erreichbar sind.

reduce :: Automat -> Automat
-- Vor.: keine
-- Erg.: In 'reduce a' ist ein zu a äquivalenter, d.h. ein die gleiche  Spra-
--       che akzeptierender Automat geliefert, der um die in a unerreichbaren
--       Zustände verkürzt ist.

writeToHaskellFile :: Automat -> [Char] -> [Char] -> IO ()
-- Vor.: Der aktuelle Benutzer hat Schreibrechte im aktuellen Verzeichnis.
-- Erg.: In 'writeToHaskellFile a name file' ist eine  Datei erzeugt, die die
--       Haskell-Definition des Automaten a, gebunden an die Variable mit dem
--       Namen name enthält.

showConfigurations :: Automat -> Word -> IO ()
-- Vor.: In 'showConfigurations a w' ist  w  in  der Kleene'schen Hülle von a
--       enthalten.
-- Erg.: Eine  Zeichenkette,  die alle Konfigurationen (qi,wi)  beginnend bei
--       der Startkonfiguration (q0,w) und endend  bei allen  Konfigurationen
--       (qi,e) mit dem leeren Wort e(psilon).
--       Konfigurationen (qi,e)  mit qi <- F sind dabei grün, solche, für die
--       das nicht gilt, rot dargestellt.

version :: IO ()
-- Vor.: keine
-- Erg.: Ein Informationstext zur aktuellen Versionsnummer ist geliefert.

-- Zur instance (Show Automat):
-- Der Automat ist als Übergangstabelle auf dem Bildschirm  angezeigt. Start-
-- zustand und Endzustände  werden wie in der  einschlägigen Literatur üblich
-- durch ein '*' bzw. '->' kenntlich gemacht. Bei NEAs wird die Zustandsmenge
-- ggf. auf  mehrere Zeilen  verteilt. Enthält der Automat keinen Endzustand,
-- ist dies durch einen  roten Stern '*' gekennzeichnet, ist  der Automat ein
-- DEA und fehlen Übergänge, erscheint der Schriftzug 'DEA' ebenfalls in rot.

-- --------------------------------------------------------------------------
-- Implementierung des ADT Automat                       (c) O.S. 2014 - 2020
-- --------------------------------------------------------------------------

--                    Q        Sigma      delta         F       q0    Typ
newtype Automat = A ([State],[[Character]],[Transition],[State],State,Atyp)
data Atyp = DEA | NEA | NEAe deriving (Show,Eq)

version = do let string = "Endliche Automaten v0.43 vom 25.01.2020"
             putStrLn string

isdea (A a) = moeglich == vorhanden
              where
              (qs,zs,ts,fs,q0,t) = a
              moeglich  = sort [(qi,z)|(qi,ti)<-qs,z<-zs]
              vorhanden = sort [(qi,z)|(qi,z,qj)<-ts]

isnea (A a) = let (qs,zs,ts,fs,q0,t) = a in t == NEA

isneaE (A a) = let (qs,zs,ts,fs,q0,t) = a in t == NEAe

dea  = newAutomat DEA
nea  = newAutomat NEA
neaE = newAutomat NEAe

newAutomat t qs zs ts
  | (not . null) (types Start qs \\ [(0,Start)]) = error error1
  | not (consistent qs' ts')                               = error error2
  | (not . and) [subset z zs|(qi,z,qj)<-ts]                = error error3
  | hasEPS ts && t /= NEAe                                 = error error4
  | otherwise = A (qs',zs',ts',fs',q0,t)
    where
    error1 = "newautomat: Nur q0 darf Startzustand sein"
    error2 = "newautomat: Zustandsliste unvollständig"
    error3 = "newautomat: Übergangsfunktion ungültig"
    error4 = "newautomat: Epsilon-Übergänge nur in NEAe erlaubt"
    qs' | length (filter ((==0) . fst) qs) > 0 = qsort qs
        | otherwise                            = qsort ((0,Start):qs)
    zs' = (f . sort . split) zs
          where f | t == NEAe = ("":)
                  | otherwise = id
    ts' = tsort t ts
    fs' = types Accept qs'
    q0  = head (filter ((==0) . fst) qs')

accepts a ws = or ((map ((==Accept) . snd)) (startFrom a 0 ws))

startFrom (A a) n ws
  | not (isdea (A a)) && t == DEA   = error error1
  | null fs                         = error error2
  | not (elem n (map fst qs))       = error error3
  | not (inKleen (split ws) zs)     = error error4
  | otherwise                       = nub (step ts (split ws) [n])
    where
    error1 = "startFrom: Automat ist kein DEA"
    error2 = "startFrom: Automat ohne Endzustand"
    error3 = "startFrom: Angegebener Zustand existiert nicht"
    error4 = "startFrom: Eingegebenes Wort nicht in Kleene'scher Hülle"
    (qs,zs,ts,fs,q0,t)    = a
    inKleen []     zs     = True
    inKleen (w:ws) []     = False
    inKleen (w:ws) (z:zs) = inKleen (filter (/=z) (w:ws)) zs
    epsShape qis | length qis /= length qis' = epsShape qis'
                 | otherwise                 = qis
                   where
                   epsStep = [qj|n<-qis,(qi,"",qj)<-ts,qi==n]
                   qis' = nub (qis ++ epsStep)
    step ts ws     []  = []
    step ts []     ixs = [(n,t)|(n,t)<-qs,i<-(ixs ++ epsShape ixs),i==n]
    step ts (w:ws) ixs
      = step' ts (w:ws) (ixs ++ epsShape ixs)
        where
        step' ts (w:ws) xs = step ts ws ((nub . concat) [nextQ ts n w|n<-xs])
 
addTransition    a (qi,z,qj) = modifyTransition a (qi,z,qj) True
changeTransition a (qi,z,qj) = modifyTransition a (qi,z,qj) False

modifyTransition (A a) (qi,z,qj) canADD
  | not (elem (head z) (concat zs))      = error error1
  | not (subset [qi,qj] (map fst qs))    = error error2
  |     canADD && transList /= []        = error error3
  | not canADD && transList == []        = error error4
  | t == DEA  && not (elem (qi,z,qj) ts) = dea  qs' (concat zs) ts'
  | t == NEA  && not (elem (qi,z,qj) ts) = nea  qs' (concat zs) ts'
  | t == NEAe && not (elem (qi,z,qj) ts) = neaE qs' (concat zs) ts'
  | canADD                               = error error5
  | not canADD                           = error error6
    where
    error1 = "modifyTransition: Übergangssymbol kein Element des Alphabets"
    error2 = "modifyTransition: Zustandsnummer(n) gibt es nicht"
    error3 = "modifyTransition: Übergang (qi,z,_) bereits vorhanden"
    error4 = "modifyTransition: Übergang (qi,z,_) existiert noch nicht"
    error5 = "modifyTransition: unvorhergesehener Fall in addTransition"
    error6 = "modifyTransition: unvorhergesehener Fall in changeTransition"
    (qs,zs,ts,fs,q0,t) = a
    transList = [(a,b,c)|(a,b,c)<-ts,a==qi,b==z]
    qs' = qs
    ts' = tsort t ((qi,z,qj):(ts \\ transList))

deleteTransition (A a) qi z
  | t == DEA  = dea  qs' (concat zs) ts'
  | t == NEA  = nea  qs' (concat zs) ts'
  | t == NEAe = neaE qs' (concat zs) ts'
  | otherwise = error "deleteTransition: unvorhergesehener Fall"
    where
    (qs,zs,ts,fs,q0,t) = a
    qs' = qs
    ts' = [(qi',z',qj')|(qi',z',qj')<-ts,not (qi'==qi && z'==z)]

addState    a (qnum,qtype) = modifyState a qnum qtype True
changeState a  qnum qtype  = modifyState a qnum qtype False

modifyState (A a) qnum qtype canADD
  |     canADD && qtype == Start && qnum /= 0   = error error1
  |     canADD && elem qnum (map fst qs)        = error error2
  | not canADD && qtype == Start                = error error3
  | not canADD && not (elem qnum (map fst qs))  = error error4
  | t == DEA                                    = dea  qs' (concat zs) ts
  | t == NEA                                    = nea  qs' (concat zs) ts
  | t == NEAe                                   = neaE qs' (concat zs) ts
  | canADD                                      = error error5
  | not canADD                                  = error error6
    where
    error1 = "modifyState: Index des Startzustandes muss 0 sein"
    error2 = "modifyState: Zustand mit gleichem Index bereits vorhanden"
    error3 = "modifyState: Automat hat bereits einen Startzustand (q0)"
    error4 = "modifyState: Zustand mit diesem Index nicht vorhanden"
    error5 = "modifyState: unvorhergesehener Fall in addState"
    error6 = "modifyState: unvorhergesehener Fall in changeState"
    (qs,zs,ts,fs,q0,t) = a
    qs' = qsort ((qnum,qtype):filter ((/=qnum) . fst) (qs \\ [(0,Start)]))

deleteState (A a) n
  | n == 0    = error error1
  | t == DEA  = dea  qs' (concat zs) ts'
  | t == NEA  = nea  qs' (concat zs) ts'
  | t == NEAe = neaE qs' (concat zs) ts'
  | otherwise = error error2
    where
    error1 = "deleteState: Startzustand lässt sich nicht löschen"
    error2 = "deleteState: unvorhergesehener Fall in deleteState"
    (qs,zs,ts,fs,q0,t) = a
    qs' = filter ((/=n) . fst) qs
    ts' = filter p ts
          where p (qi,z,qj) = qi /= n && qj /= n

states      (A (qs,zs,ts,fs,q0,t)) = qs
sigma       (A (qs,zs,ts,fs,q0,t)) = concat zs
transitions (A (qs,zs,ts,fs,q0,t)) = ts

showAccepted ads ws
  = do let string = "   " ++ pl "Wort"    ++ " | " ++  ds ++ "\n" ++
                    "   " ++ replicate ml '-' ++ "-+-" ++
                    replicate (length ds) '-' ++ "\n" ++
                    showAccepted' as ws pl pr
       putStrLn (string)
       where
       as = map fst ads
       ds = concat (map (pr . snd) ads)
       ml = max (maximum (map length ws)) (length "Wort")
       mr = max (length "nein") (maximum (map (length . snd) ads))
       pl = rjustify ml
       pr = (" "++) . (++" ") . (ljustify mr)
       showAccepted' as []     pl pr = []
       showAccepted' as (w:ws) pl pr
         = "   " ++ pl w ++ " | " ++ concat (map (pr . f) as) ++
           "\n" ++ showAccepted' as ws pl pr
           where
           f a | accepts a w = "ja  "
               | otherwise   = "nein"

instance (Show Automat) where
  show (A a)
    = heading ++ showA qs zs ts
      where
      (qs,zs,ts,fs,q0,t) = a
      nn | maximum (map fst qs) < 10 = 2
         | otherwise                 = 3
      info  = " " ++ info1 ++ replicate nn ' ' ++ info2 ++ " |"
      info1 | null fs   = vfaerben "*"  Rot
            | otherwise = vfaerben "*"  Gruen
      info2 | not (isdea (A a)) && t == DEA = vfaerben aTYP Rot
            | otherwise                 = vfaerben aTYP Gruen
              where aTYP | t == DEA  = " DEA"
                         | t == NEA  = " NEA"
                         | t == NEAe = "NEAe"
      heading = info ++ concat (map f zs) ++ "\n" ++
                " ------" ++ replicate nn '-' ++ "+" ++
                concat (replicate (length zs) ("-" ++ replicate nn '-')) ++
                "-"  ++ "\n"
                where f z | z == ""  = " " ++ rjustify nn "{}"
                          | otherwise = replicate nn ' ' ++ z
      showA []     zs ts = []
      showA (q:qs) zs ts
        = showQ q  zs (filter (not . (neg q)) ts) True ++
          showA qs zs (filter        (neg q)  ts)
          where
          neg (qnum,qtyp) (qi,z,qj) = qnum /= qi
      showQ q zs [] False = []
      showQ q zs ts isfirst
        = ftext ++ stext ++ qtext ++ "|" ++ trans ++ "\n" ++
          showQ q zs (dropfirst ts zs) False
          where
          ftext | snd q == Accept && isfirst = " * "
                | otherwise                  = "   "
          stext | fst q == 0 && isfirst = "-> "
                | otherwise             = "   "
          qtext | isfirst   = rjustify nn ("q" ++ Prelude.show (fst q)) ++ " "
                | otherwise = replicate nn '.'                            ++ " "
          trans = concat (myzip (takefirst ts zs) zs)
          myzip qs []     = []
          myzip [] (z:zs) | isfirst   = (" " ++ replicate nn '-'):myzip [] zs
                          | otherwise = (" " ++ replicate nn '.'):myzip [] zs
          myzip ((qi,z',qj):qs) (z:zs)
            | z' == z   = (" " ++ rjustify nn ("q" ++ Prelude.show qj)):myzip qs zs
            | isfirst   = (" " ++ replicate nn '-'):myzip ((qi,z',qj):qs) zs
            | otherwise = (" " ++ replicate nn '.'):myzip ((qi,z',qj):qs) zs
          dropfirst ts zs = ts \\ takefirst ts zs
          takefirst ts zs = concat [first z ts|z<-zs]
          first z []              = []
          first z ((qi,z',qj):ts) | z == z'   = [(qi,z,qj)]
                                  | otherwise = first z ts

showConfigurations a w
  = do putStrLn (headline ++ "\n" ++ sC [] w (map fst (startFrom a 0 "")) ++ bottomline)
    where
    sColor = Gelb
    iColor = Hellblau
    aColor = Gruen
    wColor = Violett
    width = max (length w) (length "Restwort")
    fill  = max (length "Restwort" - length w) 0
    zText | isdea a             = "aktueller Zustand"
          | isnea a || isneaE a = "aktuelle Zustände"
          | otherwise           = error "undefinierter Automatentyp (showConfigurations)"
    headline = rjustify (width+3) "Restwort" ++ " | " ++ zText ++ "\n" ++
               "   " ++ replicate width '-' ++ "-+-" ++ replicate 17 '-'
    bottomline = "   " ++ replicate width '-' ++ "-+-" ++ replicate 17 '-' ++ "\n" ++
                 "   " ++ cjustify (width+20+3*length (vfaerben "" Weiss))
                          (vfaerben "Start"  sColor ++ " - " ++
                           vfaerben "Inner"  iColor ++ " - " ++
                           vfaerben "Accept" aColor) ++ "\n"
    sC zs' zs     []  = []
    sC zs' []     qs' = replicate (fill+3) ' ' ++ vfaerben zs' wColor ++
                        " | " ++ showStates False qs' ++ "\n"
    sC zs' (z:zs) qs' = replicate (fill+3) ' ' ++ vfaerben zs' wColor ++ (z:zs) ++
                        " | " ++ showStates True  qs' ++ "\n" ++
                        sC (zs' ++ [z]) zs qs''
                        where
                        qs'' = (sort . nub . concat) [map fst (startFrom a q [z])|q<-qs']
    showStates t xs
      = f (showStates' t xs)
        where
        f | isnea a || isneaE a = ("{"++) . (++"}")
          | isdea a             = id
          | otherwise           = error "undefinierter Automatentyp (showConfigurations)"
        showStates' t     []       = []
        showStates' True  [x]      = vfaerben ("q" ++ Prelude.show x) (color x t)
        showStates' True  (x:y:xs) = vfaerben ("q" ++ Prelude.show x) (color x t) ++
                                     "," ++ showStates' t (y:xs)
        showStates' False [x]      = hfaerben ("q" ++ Prelude.show x) (color x t)
        showStates' False (x:y:xs) = hfaerben ("q" ++ Prelude.show x) (color x t) ++
                                     "," ++ showStates' t (y:xs)
        color x t |     t &&  styp x == Start                     = sColor
                  |     t &&  styp x == Accept                    = aColor
                  |     t &&  styp x == Inner                     = iColor
                  | not t && (styp x == Inner || styp x == Start) = Rot
                  | not t &&  styp x == Accept                    = Gruen
        styp x = (snd . head) [q|q<-states a,fst q == x]

writeToHaskellFile a name file
  = do  writeFile file astring
        where
        astring = "import LWB.EThI.Automaten\n\n" ++
                  name ++ " = " ++ atype ++ qsshow (show (states a)) ++ "\n" ++
                  tab1 ++ "   " ++ tab2  ++ show   (sigma a)         ++ "\n" ++
                  tab1 ++ "   " ++ tab2  ++ tsshow (tsort' (transitions a))
        tab1 = replicate (length name) ' '
        tab2 = replicate (length atype) ' '
        qsshow []     = error error1
        qsshow (c:cs) = qsshow' (c:cs) []
        qsshow' []           akk = akk
        qsshow' (')':',':cs) akk = qsshow' cs (akk ++ "),\n" ++ tab1 ++ "   " ++ tab2 ++ " ")
        qsshow' (c:cs)       akk = qsshow' cs (akk ++ [c])
        tsshow []     = error error2
        tsshow (t:ts) = (("["++) . (++"]")) (tsshow' ts t)
        tsshow' [] t = tshow t
        tsshow' ((qi,zs,qj):ts) (qi',zs',qj')
          | qi==qi' && qj==qj' = tsshow' ts (qi',zs'++zs,qj')
          | qi==qi' && qj==qj' = tshow (qi',zs',qj') ++ "," ++ tsshow' ts (qi,zs,qj)
          | otherwise          = tshow (qi',zs',qj') ++ ",\n" ++
                                 tab1 ++ "   " ++ tab2 ++ " " ++ tsshow' ts (qi,zs,qj)
        tshow (x,y,z) = "(" ++ Prelude.show x ++ "," ++ tochar y ++ "," ++ Prelude.show z ++ ")"
        tochar x = [toEnum 34::Char] ++ x ++ [toEnum 34::Char] -- toEnum 34::Char  ~> '"'
        atype | isdea a   = "dea "
              | isnea a   = "nea "
              | isneaE a  = "neaE "
              | otherwise = error error3
        error1 = "writeToHaskellFile: Automat ohne Zustände"
        error2 = "writeToHaskellFile: Automat ohne Übergänge"
        error3 = "writeToHaskellFile: Automatentyp undefiniert"

tsort' = foldr tinsert []
         where
         tinsert (qi,z,qj) []            = [(qi,z,qj)]
         tinsert (qi,z,qj) ((qi',z',qj'):ts)
           | qi <qi'                     = (qi,z,qj):(qi',z',qj'):ts
           | qi==qi' && qj <qj'          = (qi,z,qj):(qi',z',qj'):ts
           | qi==qi' && qj==qj' && z <z' = (qi,z,qj):(qi',z',qj'):ts
           | otherwise                   = (qi',z',qj'):tinsert (qi,z,qj) ts

powersetConstr = powersetConstr' . deleps
powersetConstr' a
  = dea qs'' zs'' ts''
    where
    qs'' = pstates (fst ps)
    zs'' = sigma a
    ts'' = snd ps
    pstates qs
      = map f qs
        where f (0,[0]) = (0,(snd . head) (filter ((==0) . fst) (states a)))
              f (n,qs)
                | null [q'|q'<-(states a),q<-qs,snd q' == Accept,fst q'==q] = (n,Inner)
                | otherwise                                                 = (n,Accept)
    -- Qi: (i,[i]),         falls qi<-states a
    -- Qi: (i,[n1,n2,...]), falls mehrelementig
    --            Qi        Ti Startindex mehrelementiger q's     Alphabet Liste zu verarbeitender Anfangszustände
    ps = powerset [(0,[0])] [] (maximum (map fst (states a)) + 1) sigma'   [[0]]
    powerset qs ts n zs     []     = (qs,ts)
    powerset qs ts n []     (m:ms) = powerset qs      ts      n     sigma' ms
    powerset qs ts n (z:zs) (m:ms)
      | not (elem q' qs') && length q' == 1 = powerset (m':qs) (t':ts) n     zs ((m:ms) ++ [q'])
      | not (elem q' qs') && length q' /= 1 = powerset (m':qs) (t':ts) (n+1) zs ((m:ms) ++ [q'])
      | otherwise                           = powerset     qs  (t':ts) n     zs  (m:ms)
        where
        qi' = head [k|(k,m'')<-qs,m''==m]
        qj' | not (elem q' qs') = fst m'
            | otherwise         = head [k|(k,m'')<-qs,m''==q']
        t' = (qi',z,qj')
        m' | length q' == 1 = (nr q',q')
           | otherwise      = (n,    q')
        q' = delta' m z
        nr [q] = q
        qs' = map snd qs
    sigma' = foldr ((:) . wrap) [] (sigma a)
             where wrap x = [x]
    delta' qs z = (sort . nub . concat) [delta q z|q<-qs]
                  where
                  delta q z = map thd3 (filter (f q z) (transitions a))
                  f qi zi (qi',zi',qj') = qi==qi' && zi==zi'
                  thd3 (x,y,z) = z

minimize' a | not (isdea a)       = error "minimize: Automat kein DEA"
            | null (equivalent a) = a
            | otherwise           = dea (states a') (sigma a') (transitions a')
              where
              a' = foldl mergeStates a (equivalent a)

minimize = reduce . minimize'

-- mergeStates a (qi,qj) fasst die beiden Zustände qi und qj zusammen. Das ist
-- nur (sinnvoll) möglich, wenn qi und qj äquivalent sind. In diesem Fall wird
-- der Zustand mit dem größeren Index gelöscht und der kleinere beibehalten.
-- Die Übergänge werden so modifiziert, dass Verweise auf die größere id in
-- Verweise auf die kleinere id geändert werden.
-- Diese Funktion kann nicht nach außen 'gereicht' werden, da beim Verschmelzen
-- nur eines äquivalenten Zustandspaares ein Zwischen-Automat entstehen kann,
-- der kein DEA ist.
mergeStates :: Automat -> (Index,Index) -> Automat
mergeStates a (qi,qj)
  = nea qs' zs' ts'
    where
    qs' = filter ((/=qi') . fst) (states a)
    zs' = sigma a
    ts' = nub (map f (transitions a))
    f (qa,z,qb) | qa==qi && qb==qi' = (qj',z,qj')
                |           qa==qi' = (qj',z,qb)
                |           qb==qi' = (qa,z,qj')
                | otherwise         = (qa,z,qb)
    qi' = max qi qj
    qj' = min qi qj

-- equivalent a liefert eine Liste von Paaren äquivalenter Zustände (qi,qj)
-- (i<j), die ggf. leer sein kann.
-- Implementierungsidee: Man markiert zuerst alle Paare (i,i) als equivalent
-- und alle Paare (i,j) mit i<j als nicht äquivalent, wenn einer der beiden
-- Zustände in der Menge der Endzustände ist und der andere nicht.
-- Nun wird für alle nicht markierten Zustandspaare untersucht, ob für
-- irgendeinen Buchstaben z des Alphabets das Paar (qi',qj') als äquivalent
-- markiert werden kann, wobei (qi,z,qi') und (qj,z,qj') Übergänge des Automaten
-- sind. Dieser Schritt wird solange wiederholt, bis sich die Menge der Markierungen
-- nicht mehr ändert. Bis zu diesem Zeitpunkt als Unknown markierte Paare sind
-- zueinander äquivalent.
equivalent a
  | not (isdea a) = error "Automat ist kein DEA (equivalent)"
  | otherwise     = equivalent' a (split (sigma a)) neqListe unListe unListe []
    where
    -- Akkubelegung:
    -- zs : Alphabetbuchstaben
    -- ns : nicht-äquivalenter Paare
    -- us : noch unverarbeitete Paare
    -- qs : unverarbeitet (aktueller Durchlauf durch Zeichenliste)
    -- qs': im aktuellen Durchlauf bereits getestet und nicht für
    --      nicht-äquivalent in diesem Schritt befunden
    equivalent' a zs     ns us []     []  = []
    equivalent' a zs     ns us []     qs'
      | us==qs'   = qs'
      | otherwise = equivalent' a sigma' ns     qs' qs'   []
    equivalent' a []     ns us (q:qs) qs'
      = equivalent' a sigma' ns     us qs     (qs' ++ [q])
    equivalent' a (z:zs) ns us (q:qs) qs'
      | elem (nQP q z ) ns = equivalent' a sigma' (q:ns) us qs     qs'
      | otherwise          = equivalent' a zs     ns     us (q:qs) qs'

    neqListe  = [(qi,qj)|qi<-accepted,qj<-naccepted,qi>qj] ++
                [(qi,qj)|qi<-naccepted,qj<-accepted,qi>qj]
    unListe   = [(qi,qj)|qi<-accepted,qj<-accepted,qi>qj]  ++
                [(qi,qj)|qi<-naccepted,qj<-naccepted,qi>qj]
    accepted  = map fst (filter ((==Accept) . snd) (states a))
    naccepted = map fst (filter ((/=Accept) . snd) (states a))
    sigma'    = split (sigma a)
    split = foldr ((:) . wrap) []
            where wrap x = [x]
    nQP (qi,qj) z | qi'>qj'   = (qi',qj')
                  | otherwise = (qj',qi')
                    where
                    qi' = (thd3 . head) (filter (delta qi z) (transitions a))
                    qj' = (thd3 . head) (filter (delta qj z) (transitions a))
                    delta q z (qi,z',qj) = q==qi && z==z'
                    thd3 (a,b,c) = c

-- --------------------------------------------------------------------------
-- Hilfsfunktionen
-- --------------------------------------------------------------------------
nextQ :: [Transition] -> Int -> [Character] -> [Int]
nextQ ts qi z = map thd (filter p ts)
                where
                thd (k1,k2,k3) = k3
                p (qi',z',qj') = qi' == qi && z' == z

types :: Typ -> [State] -> [State]
types t = filter ((==t) . snd)

qsort :: [State] -> [State]
qsort = foldr qinsert []
        where
        qinsert (n,t) [] = [(n,t)]
        qinsert (n,t) ((n',t'):qs)
          | n < n'    = (n,t):(n',t'):qs
          | n > n'    = (n',t'):qinsert (n,t) qs
          | otherwise = error "Automat mit doppeltem Zustand"

-- tsort zerlegt die Liste  der Übergänge in  eine mit epsilon-Übergängen (ts')
-- und eine mit den restlichen Übergängen (ts''), damit (sinnvolle) Eingaben in
-- der Form (qi,z1:z2:zs,qj) mögliche sind. Diese zerlegt splitT  in Übergänge,
-- die nur ein Zeichen konsumieren ((qi,z,qj)).
tsort :: Atyp -> [Transition] -> [Transition]
tsort t ts
  = foldr tinsert [] (ts' ++ (splitT [] ts''))
    where
    (ts',ts'') = splitE ts [] []
    splitE []             akk1 akk2 = (akk1,akk2)
    splitE ((qi,z,qj):ts) akk1 akk2 | z == ""   = splitE ts ((qi,z,qj):akk1) akk2
                                    | otherwise = splitE ts akk1 ((qi,z,qj):akk2)
    splitT akk []                = akk
    splitT akk ((qi,[],  qj):ts) = splitT akk ts
    splitT akk ((qi,z:zs,qj):ts) = splitT ((qi,[z],qj):akk) ((qi,zs,qj):ts)
    tinsert (qi,z,qj) [] = [(qi,z,qj)]
    tinsert (qi,z,qj) ((qi',z',qj'):ts)
      | qi  < qi'                         = (qi,z,qj):(qi',z',qj'):ts
      | qi  > qi'                         = (qi',z',qj'):tinsert (qi,z,qj) ts
      | qi == qi' && z == z' &&  t == DEA = error "Automat ist kein DEA"
      | qi == qi' && z  < z'              = (qi,z,qj):(qi',z',qj'):ts
      | qi == qi' && z  > z'              = (qi',z',qj'):tinsert (qi,z,qj) ts
      | qi == qi' && z == z' && qj  < qj' = (qi,z,qj):(qi',z',qj'):ts
      | qi == qi' && z == z' && qj  > qj' = (qi',z',qj'):tinsert (qi,z,qj) ts
      | qi == qi' && z == z' && qj == qj' = (qi',z',qj'):ts
      | otherwise                         = error "Fehler in splitT"

-- consistent liefert genau dann True, wenn es keinen Übergang gibt, an dem ein
-- nicht existierender Zustand beteiligt ist.
consistent :: [State] -> [Transition] -> Bool
consistent qs ts
  = subset ts' qs'
    where
    qs' = map fst qs
    ts' = (nub . concat) [[qi,qj]|(qi,z,qj)<-ts]

split :: [Character] -> [[Character]]
split = foldr ((:) . wrap) []
        where wrap x = [x]

subset :: Eq t => [t] -> [t] -> Bool
subset as bs = and [elem a bs|a<-as]

hasEPS :: [Transition] -> Bool
hasEPS ts = [(a,b,c)|(a,b,c)<-ts,b==""] /= []

-- Konvertierung eines NEAe in einen NEA ohne epsilon-Übergänge
-- Implementierungsidee: Die Nachfolgezustände eines über einen epsilon-Übergang
-- mit einem Zustand qi verbundenen Zustand qj werden um die von qi ergänzt. Au-
-- ßerdem müssen alle Zustände, die über einen epsilon-Übergang mit einem akzep-
-- tierenden Zustand verbunden sind, ebenfalls akzeptierende Zustände werden.
deleps a | isdea a   = error error1
--         = error "Automat hat keine Epsilon-Übergänge", if hasNoEps
         | otherwise = nea qs' zs ts'
           where
           error1 = "deleps: Automat ist ein DEA (deleps)"
           (qs,zs,ts) = (states a,sigma a,transitions a)
--           hasNoEps = [(qi,z,qj)|(qi,z,qj)<-ts;z=""] = []
           ts' = filter notEps (ts ++ epsconnect a)
--           qs' = mkset ([(qj,Accept)|qj<-q0'] ++ foldr f qs q0' -- [(0,Start)])
           qs' = nub ([(qj,Accept)|qj<-q0'] ++ foldr f qs q0')
           q0' = epsaccept a
           f qi qs = filter ((qi/=) . fst) qs

eps,notEps :: Transition -> Bool
eps (qi,z,qj) = z == ""
notEps = not . eps

-- Suche alle Zustände, die über einen Epsilon-Übergang mit einem akzeptierenden
-- Zustand verbunden sind.
epsaccept :: Automat -> [Index]
epsaccept  a    = epsaccept' a [q|(q,Accept)<-states a]
epsaccept' a fs
  | fs /= fs' = epsaccept' a fs'
  | otherwise = fs
    where
    fs' = nub (fs ++ [q|(q,t)<-states a,(q,"",qj1)<-transitions a,qj2<-fs,qj1==qj2])

-- Erstelle eine Liste von (neuen) Übergängen (qi,z,qj'), für die gilt, dass es
-- Epsilon-Übergänge (qi,"",qj) und Übergänge (qj,z,qj') gibt.
epsconnect :: Automat -> [Transition]
epsconnect  a = epsconnect' a (filter eps (transitions a)) []
epsconnect' a []              akk = akk
epsconnect' a ((qi,"",qj):ts) akk
  = epsconnect' a ts (akk ++ ts')
    where
    ts' = concat [map (f qi) (filter ((==q) . fst3) (transitions a))|q<-epsShape a qi]
    fst3 (x,y,z) = x
    f qi (x,y,z) = (qi,y,z)

epsShape :: Automat -> Index -> [Index]
epsShape  a q  = epsShape' a [q] \\ [q]
epsShape' a qs | qs' == qs = qs
               | otherwise = epsShape' a qs'
                 where
                 qs' = (sort . nub . concat) (map f qs)
                 f q = [qj|(qi,"",qj)<-transitions a,q==qi] ++ [q]

reachable :: Automat -> [Index]
reachable a
  = reachable' a (sigma a) [0]    []                [0]             (length (states a)-1)
--                         Start  aktuell erreicht  alle erreichten max. Wortlänge
    where
    alle = sort (map fst (states a))
    reachable' a (z:zs) (q:qs) akt all 0 = nub all
    reachable' a (z:zs) (q:qs) akt all n
      = reachable' a zs (q:qs) (insert q' akt) all n
        where
        q' = (fst . head) (startFrom a q [z])
        insert x []     = [x]
        insert x (y:ys) | x  < y = x:y:ys
                        | x  > y = y:insert x ys
                        | x == y = y:ys
    reachable' a [] (q:qs) akt all n
      = reachable' a (sigma a) qs akt all n
    reachable' a (z:zs) [] akt all n
      | (sort . nub) (all++akt) /= alle = reachable' a (sigma a) akt [] (all++akt) (n-1)
      | otherwise                          = alle

unreachable a = [n|(n,t)<-states a] \\ (reachable a)

reduce a = foldl deleteState a (unreachable a)
