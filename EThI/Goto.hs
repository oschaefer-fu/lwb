-- ============================ GOTO-Programme ===============================
-- (c) Oliver Schäfer                                               2016-2019
-- ===========================================================================
--
module LWB.EThI.Goto (
  Memory, Adr (..), Marke (..), Lnr (..), succ, pred, get, set, copy, goto, halt,
  ifgoto, inc, dec, noop, macro11, macro12, macro21, macro22, program, result
) where

-- goto.m stellt die Basis-Elemente einer GOTO-Sprache zur Verfügung. Ein GOTO
-- Programm hat folgende Syntax:
--
-- <name> <parameterliste> =
--   program [
--     <Liste von Marke-Anweisungspaaren, durch ',' getrennt>
--     z.B.: (N  , goto (M 2))
--     oder: (M 2, inc 4)
--   ]
-- Eine Marke <marke> kann einer von zwei Fällen sein: 'N' (keine Marke, None)
-- oder 'M <i>' (Marke i, mit i vom Typ num).
--
-- Das Ergebnis eines Programmablaufes ist der Speichervektor, der nach  Abar-
-- beitung  der letzten  Anweisung vorliegt. Jedes Programm beginnt  mit einem
-- Speichervektor 'sigmazero', in dem 16 Variablen x0,...,x15 abgelegt  werden
-- können. In der Datei 'goto.m' kann diese Konstante geändert werden.
--
-- Folgende Sprachelemente stehen zur Verfügung:
--
-- (1) Wertzuweisungen:
--     (a) Variablen belegen: xi := <c>
--         set X<i> <c>
--     (b) Variableninhalte kopieren: xj := xi
--         copy X<i> X<j>
--     (c) Erhöhen eines Variablenwertes: xi := succ (xi)
--         inc X<i>
--     (d) Erniedrigen eines Variablenwertes: xi := pred (xi)
--         dec X<i>
-- (2) Sprünge:
--     (a) Unbedingter Sprung: goto M<n>
--         goto (M <n>)
--     (b) Bedingter Sprung: if xi = 0 goto M<n>
--         ifgoto X<i> (M <n>)
-- (3) Halt-Anweisung: halt
--     halt
-- (4) Verwendung n-stelliger Makros mit einem oder zwei Rückgabewerten:
--     (a) Soll nur ein skalarer Rückgabewert (aus X0!) des Makros m verwendet
--         werden, verwendet man den Aufruf  'X<j> macro<i>1 m X<i1> [X<i2>]',
--         das den Rückgabewert nach X<j> kopiert und die/den Eingabeparameter
--         für das Makro m aus den Registern X<i1> (und X<i2>) nimmt.
--     (b) Liefert das Makro ein  Rückgabewertepaar (stets X1 und X2), so ver-
--         endet man den Aufruf 'X<j1> X<j2> macro<i>2 m X<i1> [X<i2>]'. Dabei
--         werden  die Rückgabewerte in die Register <X<j1> und X<j2> kopiert.
--     Nur die beiden Registerxn X<j1> (bzw. X<j2>) werden ggf. verändert.
-- (5) Ausgabe des Ergebnisses: x0 = f(x1,...,xn)
--     result (<name> <parameterliste>)

import Prelude hiding (succ,pred)

-- -------------------------------- Definitionen -----------------------------
type Marke       = Int
data Lnr         = N | M Marke 
                   deriving (Eq)
data Jump        = Next | First | Last | Goto Marke
                   deriving (Eq)
type Memory      = [Int]
type Instruction = Memory -> (Jump, Memory)                  -- eine Anweisung
type Codeline    = (Lnr, Instruction)
size             =  16                                        -- Speichergröße
sigmazero        =  replicate size 0                               -- Speicher
-- Speicheradressen als alg. Datentyp, ggf. an 'size' anpassen
data Adr         = X0  | X1  | X2  | X3  | X4  | X5  | X6  | X7  |
                   X8  | X9  | X10 | X11 | X12 | X13 | X14 | X15
                   deriving (Show,Enum)
-- ---------------------------------------------------------------------------

-- Um aus einer Adresse einen Speicherindex zu generieren
idx :: Adr -> Int
idx = fromEnum

-- Nachfolger- und Vorgängerfunktion (intern)
succ,pred :: Int -> Int
succ n = n+1
pred 0 = 0
pred n = n-1

-- Speicherinhalt holen (intern)
get :: Adr -> Memory -> Int
get i ram = ram!!(idx i)

-- Speicherinhalt holen und setzen in einer Anweisung (intern)
getNsetM :: Adr -> Adr -> (Int -> Int) -> Memory -> (Jump,Memory)
getNsetM adr0 adr1 p ram = set adr1 (p (get adr0 ram)) ram

-- Speicherinhalt setzen
set :: Adr -> Int -> Memory -> (Jump,Memory)
set i n ram = (Next, take (idx i) ram ++ [n] ++ drop (idx i+1) ram)

-- Diese Funktionen werden außen verwendet
copy :: Adr -> Adr -> Memory -> (Jump,Memory)
copy n m = getNsetM n m id

inc,dec :: Adr -> Memory -> (Jump,Memory)
inc  n   = getNsetM n n succ
dec  n   = getNsetM n n pred

-- halt-Anweisung
halt :: Memory -> (Jump,Memory)
halt mem = (Last, mem)

-- noop-Anweisung
noop :: Memory -> (Jump,Memory)
noop mem = (Next, mem)

-- Sprungbefehle
goto :: Lnr -> Memory -> (Jump,Memory)
goto N     mem = error "Kann nicht zu einer markenlosen Zeile springen"
goto (M n) mem = (Goto n, mem)

ifgoto :: Adr -> Lnr -> Memory -> (Jump,Memory)
ifgoto i (M n) mem
  | get i mem == 0 = goto (M n) mem
  | otherwise      = noop mem

-- Das Kernstück der Implementierung ist  das Programm, das sowohl  den
-- Speicher als auch den Programmcode verwaltet.
-- Ein Programm ist eine Anweisungsfolge, die auf einem Speicher arbei-
-- arbeitet, der zu Beginn (in  der Regel) leer ist. Die Anweisungsfol-
-- folge wird im Normalfall nacheinander in der Reihenfolge der Aufzäh-
-- lung  abgearbeitet. Eine Ausnahme macht eine goto-Anweisung, die die
-- Ausführungsreihenfolge ändern kann. Das Programm  endet  mit dem Er-
-- reichen einer halt-Anweisung.
program :: [Codeline] -> Memory
program  cs = program' (sigmazero, cs) 0
program' (ram, cs) (-1) = ram
program' (ram, cs) pc
  | action == Next  = program' (ram', cs) (pc+1)
  | action == Last  = program' (ram', cs) (-1)
  | action == First = error "unmöglicher Fall"
  | otherwise       = program' (ram', cs) pc'
    where f    = snd (cs!!pc)
          ram' = snd (f ram)
          pc'  = head [n | (n,(marke,anw))<-zip [1..] cs,
                           marke /= N, ziel action == mark marke]
          action = fst (f ram)
          ziel (Goto n) = n
          mark (M n)    = n

-- Hilfsfunktion, um das Ergebnis aus x0 zu extrahieren
result = head

-- erlaubt die Anwendung n-stelliger Makros mit n<-{1,2}, die entweder einen
-- oder zwei Rückgabewerte liefern. Im Fall eines Rückgabewertes wird dieser
-- aus X0 extrahiert, bei zwei Rückgabewerten aus X1 und X2.
macro11 out1      f in1     ram
  = set out1 (result (f (get in1 ram)))               ram
macro21 out1      f in1 in2 ram
  = set out1 (result (f (get in1 ram) (get in2 ram))) ram
macro12 out1 out2 f in1     ram
  = (set out1 (erg!!1) . snd . set out2 (erg!!2))     ram
    where erg = f (get in1 ram)
macro22 out1 out2 f in1 in2 ram
  = (set out1 (erg!!1) . snd . set out2 (erg!!2))     ram
    where erg = f (get in1 ram) (get in2 ram)
