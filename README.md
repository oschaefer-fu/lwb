# LWB: Haskell-Pakete zu endlichen Akzeptoren, regulären Ausdrücken und Berechenbarkeit

Das Paket wurde zur  Verwendung in der LWB  Informatik an der Freien Universität Berlin erstellt.
Der freie Gebrauch des Paketes für unterrichtliche Zwecke ist ausdrücklich erlaubt und erwünscht.

Zusätzlich zum Automaten-Paket werden Haskell-Pakete  zu regulären Ausdrücken RA und zur farbigen
Darstellung von Text in der Konsole sowie zur Berechenbarkeit mit LOOP-, WHILE- und GOTO-Sprachen
hinzugefügt.

    Version 0.44 von Sa 22. Feb 23:06:21 CET 2020

Installationsanweisungen:

Voraussetzung für die  Verwendung des Automatenpakets ist die vorherige Installation des
Glasgow-Haskell-Compilers. Getestet  wurde das Paket mit der Compiler-Version 7.6.3,
Aufwärtskompatibiltät sollte gewährleistet sein. Zudem  muss  eine Datei ~/.ghc/ghci.conf mit
mindestens der Zeile

    :set -i/~/ghc

angelegt sein, die den Pfad setzt, in dem dem der Interpreter beim Start nach zusätzlichen
Benutzerpaketen sucht.

Alle Dateien gehören nach ~/ghc/LWB/EThI. Hat man Vertrauen in den Autor, kann man dies durch das
Skript install erledigen lassen, ansonsten kopiert man die Haskell-Dateien per Hand dorthin. Ggf.
mass das jeweilige Verzeichnis zuerst erstellt werden.

Nach Abschluss der Installation können in  jeder Haskell-Datei die  ADTs 'Automat' und 'RA' sowie
die  Sprachen 'Loop', 'While' und 'Goto' verwendet werden, indem man sie mittels einer der Anweisungen

     import LWB.EThI.Automaten
     import LWB.EThI.RA
     import LWB.EThI.Loop
     import LWB.EThI.While
     import LWB.EThI.Goto

im aktuellen Modul zur Verfügung stellt.

Das Paket liefert einige Beispiele zu Automaten im gleichnamigen Unterordner zum Testen mit.
