module LWB.ALP1.TreeToPDF (
  btreeToPDF,
  ltreeToPDF,
  bt_,
  lt_,
  Tree(..),
  Blattbaum(..)
) where

-- =================================================================
-- (c) Oliver Sch�fer, 01.12.2019
--
-- Ausgabefunktionen f�r Bin�r- und Blattb�ume
-- erzeugt aus einer Liste von Bin�r- oder Blattb�umen mit Hilfe von
-- LaTeX TeX- und PDF-Dateien mit allen B�umen der Liste.
--
--    Beispielaufrufe:
--    ghci> btree2PDF [bt_] "test1" False
--          erzeugt nur die PDF-Datei test1.pdf
--    ghci> ltree2PDF [lt_] "test2" True
--          erzeugt TeX- und PDF-Datei test2.tex und test2.pdf
-- =================================================================

import LWB.ALP1.TreeTypes (Tree(..), Blattbaum(..))
import LWB.SysFileIO

btreeToPDF :: (Show t) => [Tree t] -> String -> Bool -> IO ()
btreeToPDF btrees name delTeX
  = do
    let all = [".log ",".aux ",".dvi "]
        tmpfiles = map (name ++) (if delTeX then ".tex ":all else all)
        bTeXtrees = map showtree btrees
        texfile = vorspann ++
                  (concat . init . concat)
                    [[bTt,pspictureBETWEEN] | bTt<-bTeXtrees] ++
                  nachspann  
    writeToFile (name ++ ".tex") texfile
    systemCall ("latex " ++ name ++ ".tex")
    systemCall ("dvipdf " ++ name ++ ".dvi")
    systemCall ("rm " ++ concat tmpfiles)
    return ()

ltreeToPDF :: (Show t) => [Blattbaum t] -> String -> IO ()
ltreeToPDF ltrees name
  = do
    let tmpfiles = map (name ++) [".tex ",".log ",".aux ",".dvi "]
    let lTeXtrees = map showblattbaum ltrees
    let texfile = vorspann ++
                  (concat . init . concat)
                    [[lTt,pspictureBETWEEN] | lTt<-lTeXtrees] ++
                  nachspann  
    writeToFile (name ++ ".tex") texfile
    systemCall ("latex " ++ name ++ ".tex")
    systemCall ("dvipdf " ++ name ++ ".dvi")
    systemCall ("rm " ++ concat tmpfiles)
    return ()

-- =================================================================
-- Anzeige einfach polymorpher Bin�rb�ume
-- Zeigt die terminierenden leeren B�ume an, wenn man \Tn durch
--    (a) \Tp (nur Linien, platzsparender, nicht vorlesungskonform)
-- oder
--    (b) \Tf (leere K�stchen wie in der Vorlesung, platzfordernder)
-- ersetzt.
showtree :: Show t => Tree t -> String
showtree t
  = pspictureIN ++ showbtree t 0 ++ pspictureOUT
    where
    showbtree E acc = sp acc ++"\\Tf\n"  -- Blattausgabe
    showbtree (N l w r) acc 
      = sp acc ++"\\pstree{\\Tcircle{"++ show w ++ "}}{%\n"++
        showbtree l (acc+1) ++ 
        showbtree r (acc+1) ++ sp acc++"}\n" 

-- =================================================================
-- Anzeige von bin�ren Blattb�umen (leaf trees)
-- data Blattbaum t = B t | Y (Blattbaum t) (Blattbaum t) deriving (Show)
showblattbaum :: (Show t) => Blattbaum t -> String
showblattbaum t
  = pspictureIN ++ showblattbaum' t 0 ++ pspictureOUT
    where
    -- Blattausgabe
    showblattbaum' (B k) acc = sp acc ++ "\\Tcircle{" ++ show k ++ "}\n"
    -- Innere (leere) Knoten
    showblattbaum' (Y l r) acc
      = sp acc ++ "\\pstree{\\Tdot}{%\n" ++
        showblattbaum' l (acc+1) ++ 
        showblattbaum' r (acc+1) ++
        sp acc ++ "}\n" 

-- =================================================================
-- TeX-Definitionen von Vor- und Nachspann einer Minimaldatei
vorspann, nachspann, pspictureIN, pspictureOUT, pspictureBETWEEN :: String
vorspann 
  = "\\documentclass[a4paper,10pt]{article}\n" ++
    "\\usepackage[includeheadfoot,landscape,left=2mm,right=2mm]{geometry}\n" ++
    "\\usepackage[latin1]{inputenc}\n" ++
    "\\usepackage{pst-tree}\n" ++
    "\\begin{document}\n"
nachspann
  = "\\end{document}\n"
pspictureIN
  = "\\begin{center}\n" ++
    "\\begin{pspicture}(0,0)(20,-4)\n" ++
    "\\psset{levelsep=1cm}\n"          
pspictureOUT
  = "\\end{pspicture}\n" ++
    "\\end{center}\n"
pspictureBETWEEN
  = "\\clearpage\n"

-- =================================================================
-- Hilfsfunktion
sp n = replicate (2*n) ' '

-- =================================================================
-- Testb�ume
bt_  = N (N (N E 2 E) 3 (N E 4 E)) 5 (N (N E 7 E) 8 (N E 9 E))
lt_  = Y (Y (B 2) (B 4)) (Y (B 7) (B 9))
