-- =========== (c) Oliver Schäfer, 24.11.2019 ============
-- Modul zum Schreiben in und Lesen aus (Text-)Dateien
-- sowie zur Ausführung von Systemkommandos mit Parametern
-- =======================================================

module LWB.ALP1.SysFileIO (writeToFile, readFromFile, systemCall) where

import System.IO
import System.Process (readProcessWithExitCode)
import System.Exit (ExitCode(..))

-- writeToFile file text schreibt die Zeichenkette text in eine Datei
-- file. Existiert die Datei file bereits, wird sie überschrieben.
-- Schreibrechte des aktuellen Benutzers im Zielverzeichnis müssen
-- gegeben sein.
writeToFile :: String -> String -> IO ()
writeToFile file text
  = do
    handle <- openFile file WriteMode
    hPutStrLn handle text
    hClose handle

-- readFromFile file liest den Inhalt der Datei file in eine Zeichenkette
-- ein und liefert diese an den Aufrufer zurück.
readFromFile :: String -> IO String
readFromFile file
  = do
    handle <- openFile file ReadMode
    lines <- hGetContents handle
    return lines

-- Nachbildung der gleichnamigen Funktion aus der Miranda-Standardumgebung
-- Der Eingabeparameter wird entlang von Leerzeichen zerlegt, das Kopfelement
-- als Prozessname und die weiteren Elemente als Aufrufparameter der
-- Prozesses an diesen weitergegeben.

systemCall :: [Char] -> IO ([Char],[Char],Int)
systemCall args
  = do
    let xss = invConcat ' ' args
    ret <- readProcessWithExitCode (head xss) (tail xss) []
    let (exit,stdout,stderr) = ret
        exitcode = case exit of
                     ExitSuccess   -> 0
                     ExitFailure n -> n
    return (stdout,stderr,exitcode)

-- nicht exportierte Hilfsfunktionen
invConcat :: Eq t => t -> [t] -> [[t]]
invConcat c xs
  = invConcat' c xs []
    where
    invConcat' c []     akk = [akk]
    invConcat' c (x:xs) akk | x == c    = akk:invConcat' c xs []
                            | otherwise = invConcat' c xs (akk ++ [x])
