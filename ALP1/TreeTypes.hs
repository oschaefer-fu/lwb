-- Verschiedene Baumtypen
module LWB.ALP1.TreeTypes where

-- Binärbäume
data Tree t = E | N (Tree t) t (Tree t)
              deriving (Show)

-- Blattbäume
data Blattbaum t = B t | Y (Blattbaum t) (Blattbaum t)
                   deriving (Show)

-- Operatorbäume
data Optree t1 t2 = Z t1 | O (Optree t1 t2) t2 (Optree t1 t2)
                    deriving (Show)

-- Vielwegbäume
data VBaum t = V t [VBaum t]
               deriving (Show)
data MTree t = M t [MTree t]
               deriving (Show)

-- Ternärbäume
data Terbaum t = L | T t (Terbaum t) (Terbaum t) (Terbaum t)
                 deriving (Show)
