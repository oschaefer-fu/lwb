module LWB.ALP1.AVLBaeume (
  Tree (..),
  einfuegen,
  entfernen,
  einfuegen',
  entfernen',
  istAVL,
  rotL,
  rotR,
  rotRL,
  rotLR
) where

import LWB.Utils.TextFormat

------------ Tree als algebraischer Datentyp ----------------------

data Tree t where
  E:: Ord t => Tree t
  N:: Ord t => Tree t -> t -> Tree t -> Tree t

deriving instance Eq t => (Eq (Tree t))
  
-- Anzeigefunktion für Binärbäume in üblicher Darstellung von oben nach unten.
-- Ursprüngliche Version von F. Weber, verschlankt und ergänzt von O. Schäfer.
-- AVL-Bäume werden dabei in grün, nicht-AVL-Bäume in rot dargestellt.
instance (Ord t, Show t) => Show (Tree t) where
  show t
    | t == E    = vfaerben ("\n" ++ "leer" ++ "\n")     Gruen
    | istAVL t  = vfaerben ("\n" ++ show' (ihoehe t) t) Gruen
    | otherwise = vfaerben ("\n" ++ show' (ihoehe t) t) Rot
      where
      show' 0 t = matrixLine value 0 t
      show' n t = (show' (n-1) t)      ++ "\n" ++
                  matrixLine edge  n t ++ "\n" ++
                  matrixLine value n t ++ "\n"
      --
      matrixLine f n t = toLine (map f (filter (line n) (posList t)))
      toLine [] = ""
      toLine [(b, w)] = tabStr b ++ w
      toLine ((b1,w1):(b2,w2):zs) = toLine ((b2,w2):zs) ++ tabStr (b1-b2) ++ w1
      -- 
      tabStr n = replicate n '\t'
      --tabStr n = concat (replicate n "  ")
      -- posList ergänzt die Knoten des Baumes um Informationen zur Position
      -- in der Darstellungsmatrix und die Kanten zu den Unterbäumen
      -- (Linkskante '/' bzw. Rechtskante '\') und liefert eine inorder-tra-
      -- versierte Liste in umgekehrter Reihenfolge für die Ausgabe.
      posList t
        = posList' t (0,2^(ihoehe t)-1,"")
          where
          posList' E akk = []
          posList' (N l w r) (h,b,z) = 
            posList' r (h+1, b+(2^(ihoehe (N l w r) - 1)),"\\") ++
            [(h,b,show w,z)]                                    ++
            posList' l (h+1, b-(2^(ihoehe (N l w r) - 1)),"/") 
      --
      edge    (_,b,_,e) = (b,e)
      line  n (h,_,_,_) = n == h
      value   (_,b,v,_) = (b,v)
      --
      ihoehe E = 0
      ihoehe (N E w E) = 0
      ihoehe (N l w r) = 1 + max (ihoehe l) (ihoehe r)

------------------------------------------------------------------------------------

hoehe :: Tree t -> Int
hoehe  E        = 0
hoehe (N l w r) = 1 + max (hoehe l) (hoehe r)

istAVL :: Ord t => Tree t -> Bool
istAVL  E        = True
istAVL (N l w r) = istAVL l && istAVL r && abs (hoehe l - hoehe r) < 2

rotL, rotR, rotLR, rotRL :: Ord t => Tree t -> Tree t
rotL  (N l w (N rl rw rr)) = N (N l w rl) rw rr
rotR  (N (N ll lw lr) w r) = N ll lw (N lr w r)
rotLR (N l w r) = rotR (N (rotL l) w r)
rotRL (N l w r) = rotL (N l w (rotR r))

einfuegen :: Ord t => Tree t -> t -> Tree t
einfuegen E x = N E x E
einfuegen (N l w r) x
  | x <  w = ausgleichen (N (einfuegen l x) w r)
  | x >  w = ausgleichen (N l w (einfuegen r x))
  | x == w = N l w r

einfuegen' :: Ord t => Tree t -> t -> Tree t
einfuegen' E x = N E x E
einfuegen' (N l w r) x
  | x <  w = N (einfuegen' l x) w r
  | x >  w = N l w (einfuegen' r x)
  | x == w = N l w r

ausgleichen :: Ord t => Tree t -> Tree t
ausgleichen b@(N l w r)
  | abs (balance b) < 2                = b
  | balance b == -2 && balance r /=  1 = rotL  b
  | balance b ==  2 && balance l /= -1 = rotR  b
  | balance b == -2 && balance r ==  1 = rotRL b
  | balance b ==  2 && balance l == -1 = rotLR b
    where balance (N l w r) = hoehe l - hoehe r

entfernen' :: Ord t => Tree t -> t -> Tree t
entfernen' E x = E
entfernen' (N E w E) x | x == w = E
entfernen' (N l w E) x | x == w = l
entfernen' (N E w r) x | x == w = r
entfernen' (N l w r) x
  | x <  w           = N (entfernen' l x) w r
  | x >  w           = N l w (entfernen' r x)
  | otherwise        = N (entfernen' l m) m r
    where m = maxEl l

entfernen :: Ord t => Tree t -> t -> Tree t
entfernen E x = E
entfernen (N E w E) x | x == w = E
entfernen (N l w E) x | x == w = l
entfernen (N E w r) x | x == w = r
entfernen (N l w r) x
  | x <  w           = ausgleichen (N (entfernen l x) w r)
  | x >  w           = ausgleichen (N l w (entfernen r x))
  | otherwise        = ausgleichen (N (entfernen l m) m r)
    where m = maxEl l

maxEl :: Tree t -> t
maxEl (N l w E) = w
maxEl (N l w r) = maxEl r
