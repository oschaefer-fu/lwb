{-
  (c) Oliver Schäfer                           Mi 2. Okt 07:14:41 CEST 2019

  Zufallsmodul für die Verwendung im Rahmen der funktionalen Programmierung
  Implementierung in Haskell auf der Basis des gleichnamigen Miranda-Moduls
  aus den Jahren 2012-2015.
  In Haskell stehen Zufallszahlen direkt zur Verfügung, hier werden sie je-
  doch nur zur Initialisierung des LKG-Keims verwendet, die eigentliche Er-
  zeugung der Zufallszahlen erfolgt mit einem  explizit implementierten LGK
  auf der Basis einer Version, die im CAS Derive verwendet wird.
  In Haskell muss das  Monadenkonzept eingesetzt werden, wenn man bei jedem
  Aufruf  eine neue Liste  von Zufallszahlen erhalten möchte. Daher gibt es
  die Zufallsfunktionen, die Listen liefern immer in zwei Varianten:

  ( I) intlist, ratlist, keylist:        liefern Pseudo-Zufallszahlen
  (II) intlistIO, ratlistIO, keylistIO:  liefern "echte" Zufallszahlen

  Zur Verfügung gestellt werden die folgenden Funktionen/Aktionen:

  (1)  intIO max :: Int -> IO Int
       liefert eine zufällige ganze Zahl, die durch den Wert von <max> nach
       oben beschränkt ist.
  (2)  key len :: Int -> IO Int
       liefert einen zufälligen,ganzzahligen Schlüssel aus <len> Ziffern
  (3)  rat :: IO Float
       liefert eine rationale Zufallszahl z aus dem Intervall [0;1[.
  (4)  intlist s t max anz :: STyp -> KTyp -> Int -> Int -> [Int]
       liefert eine Liste von <anz> ganzzahligen Zufallszahlen mit der obe-
       ren Schranke <max>. Duplikate sind möglich.
       Der Wert des Parameters <t> steuert den Keim des LKG:
       - Default: Es wird ein Defaultwert für den Keim verwendet, die Liste
                  enthält bei jedem Aufruf die gleichen Zahlen.
       - Keim k:  Der LKG wird mit dem Keim k initialisiert. Die Liste ent-
                  hält bei jedem Aufruf mit gleichem k die gleichen Zahlen.
       Der Wert des Parameters <s> steuert die Vorsortierung der Zahlen:
       - Auf:     Zahlen sind AUFSTEIGEND vorsortiert
       - Ab:      Zahlen sind ABSTEIGEND  vorsortiert
       - Nicht:   Zahlen sind NICHT       vorsortiert
  (4a) intlistIO s t' max anz :: STyp -> ZTyp -> Int -> Int -> IO [Int]
       Arbeitet wie intlist, liefert aber ein monadisches Ergebnis.
       Der Wert des Parameters t' steuert die Art des Zufalls:
       - Pseudo:  Es werden immer die gleichen Zahlen geliefert, nämlich
                  diejenigen, die intlist mit dem Default-Keim liefert.
       - Random:  Es werden bei jedem Aufruf neue Zahlen geliefert. Der
                  Keim wird dazu über die Bibliothek System.Random ermit-
                  telt.
  (5)  keylist s t len anz :: STyp -> KTyp -> Int -> Int -> [Int]
       liefert eine Liste von <anz> Schlüsseln, mit (genau) <len>  Ziffern.
       Es ist len < 10. In der Liste kommen - gemäß Schlüsseleigenschaft -
       keine Werte  doppelt vor. Für anz >= 10^len wartet man ewig.
       Zur Bedeutung von <s> bzw. <t> siehe 'intlist'
  (5a) keylistIO s t' max anz :: STyp -> ZTyp -> Int -> Int -> IO [Int]
       Beschreibung siehe keylist bzw. intlistIO'
  (6)  ratlist s t n :: STyp -> KTyp -> Int -> [Float]
       liefert eine Liste mit <n> Zufallszahlen z aus dem Intervall [0;1[.
       Zur Bedeutung von <s> bzw. <t> siehe 'intlist'
  (6a) ratlistIO :: STyp -> ZTyp -> Int -> IO [Float]
       Beschreibung siehe ratlist bzw. intlistIO'
  (7)  time f :: NFData t => t -> IO POSIXTime
       Wertet den Ausdruck f aus und gibt  die dafür benötigte Zeit in Se-
       kunden zurück. Der ausgewertete Ausdruck wird verworfen.
  (8)  showtime f :: (NFData t, Show t) => t -> IO (t,POSIXTime)
       Gibt das Paar (f,t) aus mit t = time f.
-}

module LWB.ALP1.ZufallIO (
  KTyp (..), STyp (..), ZTyp (..),
  intlist, intlistIO, intIO,
  keylist, keylistIO, keyIO,
  ratlist, ratlistIO, ratIO,
  showtime, time
) where

import Data.Time.Clock.POSIX
import Data.List
import System.Random
import Control.DeepSeq

-- Keimtyp
data KTyp = Default | Keim Integer deriving (Show,Eq)
-- Zufallstyp
data ZTyp = Pseudo | Random deriving (Show,Eq)
-- Sortiertyp
data STyp = Auf | Ab | Nicht deriving (Show,Eq)

-- intlistIO realisiert Zufallszahlen in einem vorgegebenen Bereich
-- durch Rückgriff auf das Paket System.Random
intlistIO :: STyp -> ZTyp -> Int -> Int -> IO [Int]
intlistIO s t m n = do
  if t == Pseudo then do
    return $! (intlist s Default m n)
  else do
    keim <- getStdRandom $ randomR (1,1000000000)
    return $! (intlist Nicht (Keim keim) m n)

-- Liste mit Zufallszahlen unterhalb einer gegebenen Schranke
-- geändert (10/2019): auf- und absteigend vorsortierte Daten
-- werden als arithmetische Listen geliefert.
intlist :: STyp -> KTyp -> Int -> Int -> [Int]
intlist s t m n
  | s == Nicht = list
  | s == Auf   = [1..n]
  | s == Ab    = [n,n-1..1]
         where
         list = take n (map ((`mod` m) . fromIntegral) (lkg t))

-- showtime f liefert das  Paar (f,time),  wobei t die Zeit (in Sekunden)
-- ist, die für die Ausführung von f benötigt wurde.
showtime :: (NFData t, Show t) => t -> IO (t,POSIXTime)
showtime f = do
  t0 <- getPOSIXTime
  t1 <- deepseq f getPOSIXTime
  return (f,t1-t0)

-- 'time f' liefert die Ausführungszeit der Funktion f. Das Ergebnis von
-- f wird dabei verworfen.
time :: NFData t => t -> IO POSIXTime
time f = do
  t0 <- getPOSIXTime
  t1 <- deepseq f getPOSIXTime
  return (t1-t0)

-- Liste mit Zufallsschlüsseln mit fester Ziffernanzahl
keylist :: STyp -> KTyp -> Int -> Int -> [Int]
keylist s t l n
  | s == Nicht = list
  | s == Auf   = sort list
  | s == Ab    = (reverse . sort) list
    where
    list = takekeys n [] (map (read . (take l) . show) (lkg t))
    takekeys 0 akku (x:xs) = akku
    takekeys n akku (x:xs) | elem x akku = takekeys n     akku     xs
                           | otherwise   = takekeys (n-1) (x:akku) xs

-- keylistIO realisiert Schlüssel vorgegebener Länge
-- durch Rückgriff auf das Paket System.Random
keylistIO :: STyp -> ZTyp -> Int -> Int -> IO [Int]
keylistIO s t m n = do
  if t == Pseudo then
    return (keylist s Default m n)
  else do
    keim <- getStdRandom $ randomR (1,1000000000)
    return (keylist Nicht (Keim keim) m n)

-- Liste mit rationalen Zufallszahlen
ratlist :: STyp -> KTyp -> Int -> [Float]
ratlist s t n
  | s == Nicht = list
  | s == Auf   = sort list
  | s == Ab    = (reverse . sort) list
    where
    list = map ((*faktor) . fromIntegral) (intlist s t modul n)
    modul  = 2^29 :: Int
    faktor = 1/2^29 :: Float

-- ratlistIO realisiert Zufallszahlen zwischen 0 und 1
-- durch Rückgriff auf das Paket System.Random
ratlistIO :: STyp -> ZTyp -> Int -> IO [Float]
ratlistIO s t n = do
  if t == Pseudo then
    return (ratlist s Default n)
  else do
    keim <- getStdRandom $ randomR (1,1000000000)
    return (ratlist Nicht (Keim keim) n)

-- Ganze Zufallszahl i mit 0 <= i < n
intIO :: Int -> IO Int
intIO n = do
  list <- intlistIO Nicht Random n 1
  return (list!!0)

-- zufälliger Schlüssel mit vorgegebener Stellenanzahl
keyIO :: Int -> IO Int
keyIO n = do
  list <- keylistIO Nicht Random n 1
  return (list!!0)

-- rationale Zufallszahl q mit 0 <= q < 1
ratIO :: IO Float
ratIO = do
  list <- ratlistIO Nicht Random 1
  return (list!!0)

-- *************************** Hilfsfunktionen *****************************
-- LKG, entnommen aus random.mat.sbg.ac.at/results/karl/server (DERIVE)
lkg :: KTyp -> [Integer]
lkg x = lkg' (2^32) 3141592653 1 (keim x)
        where lkg' m a b y0 = y0:lkg' m a b ((a*y0+b) `mod` m)

keim :: KTyp -> Integer
keim Default  = 13072019 -- Entwicklungsdatum des Moduls
keim (Keim k) = k

readInt :: IO Int
readInt = readLn
-- *************************************************************************
